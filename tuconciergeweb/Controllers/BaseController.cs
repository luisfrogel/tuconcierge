﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tuconciergemodels;

namespace tuconciergeweb.Controllers
{
    public abstract class BaseController : Controller
    {
        #region Properties

        protected string ApiDomain { get; set; }

        private readonly Dictionary<string, object> Headers;

        private readonly List<Parameter> Parameters;

        private string Body;

        private HttpPostedFileBase[] Files;

        #endregion

        #region Constructors

        public BaseController()
        {
            ApiDomain = ConfigurationManager.AppSettings["ApiDomain"].ToString();

            Headers = new Dictionary<string, object>();

            Parameters = new List<Parameter>();

            Body = "";

            Files = null;
        }

        #endregion

        #region Protected Setters

        protected void SetHeader(string key, object value)
        {
            Headers.Add(key, value);
        }

        protected void SetAuthentication()
        {
            Headers.Add("Authorization", string.Format("{0} {1}", Session["token_type"], Session["access_token"]));
        }

        protected void SetParameter(Parameter item)
        {
            Parameters.Add(item);
        }

        protected void SetBody(object model)
        {
            Body = JsonConvert.SerializeObject(model);
        }

        protected void SetFiles(HttpPostedFileBase[] fileBases)
        {
            Files = fileBases;
        }

        protected bool SessionActive()
        {
            if(Session["token_type"] != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region Protected Methods

        protected IRestResponse SendRequest(Method method, string endPoint, Enums.Authentication authentication = Enums.Authentication.NONE)
        {
            RestRequest request = new RestRequest(endPoint, method);

            switch (authentication)
            {
                case Enums.Authentication.NONE:
                    break;
                case Enums.Authentication.DEFAULT:
                    SetAuthentication();
                    break;
            }

            foreach (var key in Headers.Keys)
            {
                if (Headers[key].GetType().ToString().StartsWith("System.Collections.Generics.List"))
                    request.AddHeader(key, JsonConvert.SerializeObject(Headers[key]));
                else
                    request.AddHeader(key, Headers[key].ToString());
            }

            foreach (var param in Parameters)
            {
                request.AddParameter(param);
            }

            if (Body != "")
            {
                request.AddParameter("application/json", Body, ParameterType.RequestBody);
            }

            if (Files != null)
            {

                foreach (var file in Files)
                {
                    MemoryStream target = new MemoryStream();
                    file.InputStream.CopyTo(target);
                    request.AddFile("files", target.ToArray(), "FileName");
                }

                request.AlwaysMultipartFormData = true;
            }

            var response = Execute(request);

            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                var refreshResponse = RefreshToken();

                if (refreshResponse.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    return refreshResponse;
                }
                else
                {
                    SetRefreshData(refreshResponse.Content);
                    request.AddOrUpdateParameter("Authorization", string.Format("{0} {1}", Session["token_type"], Session["access_token"]));
                    var execAgain = Execute(request);
                    ClearProperties();
                    return execAgain;
                }
            }
            else
            {
                ClearProperties();
                return response;
            }
        }

        protected void ClearSession()
        {
            Session.RemoveAll();
        }

        public JsonResult ValidateResponse(IRestResponse response)
        {
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return Json(new IResponseActions
                {
                    Success = true,
                    Title = "Operación exitosa",
                    Message = "Se ha procesado correctamente",
                    TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt")
                }, JsonRequestBehavior.AllowGet);
            }
            else if (response.StatusCode == HttpStatusCode.Created)
            {
                return Json(new IResponseActions
                {
                    Success = true,
                    Title = "Operación exitosa",
                    Message = "La información ha sido agregada correctamente.",
                    TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt")
                }, JsonRequestBehavior.AllowGet);
            }
            else if (response.StatusCode == HttpStatusCode.NoContent)
            {
                return Json(new IResponseActions
                {
                    Success = true,
                    Title = "Operación exitosa",
                    Message = "La información ha sido actualizada correctamente.",
                    TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt")
                }, JsonRequestBehavior.AllowGet);
            }
            else if (response.StatusCode == HttpStatusCode.Accepted)
            {
                return Json(new IResponseActions
                {
                    Success = true,
                    Title = "Operación exitosa",
                    Message = "La información ha sido dada de baja correctamente.",
                    TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt")
                }, JsonRequestBehavior.AllowGet);
            }
            else if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                //var data = JsonConvert.DeserializeObject<T>(response.Content);

                return Json(new IResponseItems<object>
                {
                    Success = false,
                    //Data = data,
                    TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt")
                }, JsonRequestBehavior.AllowGet);
            }
            else if (response.StatusCode == HttpStatusCode.NotFound)
            {
                return Json(new IResponseActions
                {
                    Success = false,
                    Title = "No encontrado",
                    Message = "La información solicitada no ha sido encontrada.",
                    TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt")
                }, JsonRequestBehavior.AllowGet);
            }
            else if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                return Json(new IResponseActions
                {
                    Success = false,
                    Title = "Acceso denegado",
                    Message = "Inicie sesión para continuar.",
                    TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt")
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new IResponseActions
                {
                    Success = false,
                    Title = "Acceso denegado",
                    Message = "Inicie sesión para continuar.",
                    TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt")
                }, JsonRequestBehavior.AllowGet);
            }

        }

        #endregion

        #region Private Methods

        private IRestResponse Execute(RestRequest request)
        {
            RestClient client = new RestClient(ApiDomain);

            var response = client.Execute(request);

            return response;
        }

        private IRestResponse RefreshToken()
        {
            var endPoint = "/api/v1/login";
            RestRequest request = new RestRequest(endPoint, Method.POST);
            request.AddParameter(new Parameter("grant_type", "refresh_token", ParameterType.GetOrPost));
            request.AddParameter(new Parameter("refresh_token", Session["refresh_token"], ParameterType.GetOrPost));
            request.AddParameter(new Parameter("client_id", "", ParameterType.GetOrPost));
            return Execute(request);
        }

        private void SetRefreshData(string response)
        {
            dynamic responseDeserialized = JsonConvert.DeserializeObject<object>(response);

            var sessionToken = new ITokenModel()
            {
                AccessToken = responseDeserialized.access_token,
                TokenType = responseDeserialized.token_type,
                RefreshToken = responseDeserialized.refresh_token,
                UserName = responseDeserialized.userName
            };

            Session["access_token"] = sessionToken.AccessToken;
            Session["token_type"] = sessionToken.TokenType;
            Session["refresh_token"] = sessionToken.RefreshToken;
            Session["userName"] = sessionToken.UserName;
        }

        private void ClearProperties()
        {
            Headers.Clear();

            Parameters.Clear();

            Body = "";

            Files = null;
        }

        #endregion
    }
}