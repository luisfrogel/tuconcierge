﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tuconciergemodels;
using tuconciergeweb.Interfaces;

namespace tuconciergeweb.Controllers
{
    public class CoveragesController : BaseController
    {
        private readonly string EndPoint = "/api/Coverages";

        // CONSTRUCTOR
        public CoveragesController() : base() { }

        public ActionResult ConfirmDelete(long id)
        {
            var response = SendRequest(Method.GET, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<CoverageViewModel>(response.Content);

            return PartialView(model);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            try
            {
                SetParameter(new Parameter("id", id, ParameterType.UrlSegment));

                var response = SendRequest(Method.DELETE, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

                return ValidateResponse(response);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, errors = ex }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Details(long id)
        {
            SetParameter(new Parameter("id", id, ParameterType.UrlSegment));

            var response = SendRequest(Method.GET, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<CoverageViewModel>(response.Content);

            return PartialView(model);
        }

        public ActionResult Form(long id = 0)
        {
            if (id == 0)
            {
                ViewBag.ModalTitle = "Agregar covertura";
                ViewBag.SaveTextButton = "Guardar";
                return PartialView(new CoverageViewModel());
            }

            SetParameter(new Parameter("id", id, ParameterType.UrlSegment));

            var response = SendRequest(Method.GET, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<CoverageViewModel>(response.Content);

            ViewBag.ModalTitle = "Editar covertura";
            ViewBag.SaveTextButton = "Actualizar";
            return PartialView(model);
        }

        // GET: Coverages
        public ActionResult Index()
        {
            if (!SessionActive())
            {
                return Redirect("/Home/Login");
            }

            return View();
        }

        public ActionResult List()
        {
            if (Request.QueryString["Name"] != null)
            {
                var name = Request.QueryString["Name"].ToString();

                SetParameter(new Parameter("Name", name, ParameterType.QueryString));
            }

            if (Request.QueryString["OfficeId"] != null)
            {
                var OfficeId = Request.QueryString["OfficeId"].ToString();

                SetParameter(new Parameter("OfficeId", OfficeId, ParameterType.QueryString));
            }

            var response = SendRequest(Method.GET, EndPoint, Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<IEnumerable<CoverageViewModel>>(response.Content);

            return PartialView(model);
        }

        public JsonResult Save(Coverage model)
        {
            try
            {
                if (model.Id != 0)
                {
                    SetParameter(new Parameter("id", model.Id, ParameterType.UrlSegment));

                    SetBody(model);

                    var response = SendRequest(Method.PUT, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

                    return ValidateResponse(response);

                }
                else
                {
                    SetBody(model);

                    var response = SendRequest(Method.POST, EndPoint, Enums.Authentication.DEFAULT);

                    return ValidateResponse(response);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, errors = ex }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}