﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tuconciergemodels;

namespace tuconciergeweb.Controllers
{
    public class AccountController : BaseController
    {
        private readonly string EndPoint = "/api/Account";

        // GET: Account
        public ActionResult Activation(string Email, string Code)

        {
            VerifyEmailModel model = new VerifyEmailModel()
            {
                Email = Email,
                Code = Code
            };

            return View(model);
        }

        [HttpPost]
        public JsonResult Activate(VerifyEmailModel model)
        {
            try
            {
                SetBody(model);

                var response = SendRequest(Method.POST, EndPoint + "/VerifyEmail", Enums.Authentication.DEFAULT);

                return ValidateResponse(response);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, errors = ex }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}