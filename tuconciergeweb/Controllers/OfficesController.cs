﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tuconciergemodels;
using tuconciergeweb.Interfaces;
using tuconciergeweb.Services;

namespace tuconciergeweb.Controllers
{
    public class OfficesController : BaseController
    {
        private readonly string EndPoint = "/api/Offices";

        // CONSTRUCTOR
        public OfficesController() : base() { }

        public ActionResult ConfirmDelete(long id)
        {
            var response = SendRequest(Method.GET, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<OfficeViewModel>(response.Content);

            return PartialView(model);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            try
            {
                SetParameter(new Parameter("id", id, ParameterType.UrlSegment));

                var response = SendRequest(Method.DELETE, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

                return ValidateResponse(response);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, errors = ex }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Details(long id)
        {
            SetParameter(new Parameter("id", id, ParameterType.UrlSegment));

            var response = SendRequest(Method.GET, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<OfficeViewModel>(response.Content);

            return PartialView(model);
        }

        public ActionResult Location(long id)
        {
            SetParameter(new Parameter("id", id, ParameterType.UrlSegment));

            var response = SendRequest(Method.GET, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<OfficeViewModel>(response.Content);

            ViewBag.ModalTitle = "Establecer ubicación";
            ViewBag.SaveTextButton = "Guardar";
            return PartialView(model);
        }

        [HttpPost]
        public JsonResult SaveLocation(Office model)
        {
            SetParameter(new Parameter("id", model.Id, ParameterType.UrlSegment));

            SetBody(model);

            var response = SendRequest(Method.PUT, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

            return ValidateResponse(response);
        }

        public ActionResult Form(long id = 0)
        {
            if (id == 0)
            {
                ViewBag.ModalTitle = "Agregar sucursal";
                ViewBag.SaveTextButton = "Guardar";
                return PartialView(new OfficeViewModel());
            }

            SetParameter(new Parameter("id", id, ParameterType.UrlSegment));

            var response = SendRequest(Method.GET, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<OfficeViewModel>(response.Content);

            ViewBag.ModalTitle = "Editar sucursal";
            ViewBag.SaveTextButton = "Actualizar";
            return PartialView(model);
        }


        // GET: Countries
        public ActionResult Index()
        {
            if (!SessionActive())
            {
                return Redirect("/Home/Login");
            }

            return View();
        }

        public ActionResult List()
        {
            if (Request.QueryString["Name"] != null)
            {
                var name = Request.QueryString["Name"].ToString();

                SetParameter(new Parameter("Name", name, ParameterType.QueryString));
            }

            var response = SendRequest(Method.GET, EndPoint, Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<IEnumerable<OfficeViewModel>>(response.Content);

            return PartialView(model);
        }

        [HttpPost]
        public JsonResult Save(HttpPostedFileBase file, Office model)
        {
            try
            {
                if(file != null)
                {
                    var fileUpload = new FileUploadService();

                    model.Picture = fileUpload.ImageUpload(file);
                }

                if (model.Id != 0)
                {
                    SetParameter(new Parameter("id", model.Id, ParameterType.UrlSegment));

                    SetBody(model);

                    var response = SendRequest(Method.PUT, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

                    return ValidateResponse(response);

                }
                else
                {
                    SetBody(model);

                    var response = SendRequest(Method.POST, EndPoint, Enums.Authentication.DEFAULT);

                    return ValidateResponse(response);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, errors = ex }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Select()
        {
            var response = SendRequest(Method.GET, EndPoint, Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<List<Office>>(response.Content);

            return PartialView(model);
        }
    }
}