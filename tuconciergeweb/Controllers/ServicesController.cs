﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tuconciergemodels;
using tuconciergeweb.Interfaces;

namespace tuconciergeweb.Controllers
{
    public class ServicesController : BaseController, IViewController<Service>
    {
        private readonly string EndPoint = "/api/Services";

        public ServicesController() : base() { }

        public ActionResult ConfirmDelete(long id)
        {
            SetParameter(new Parameter("id", id, ParameterType.UrlSegment));

            var response = SendRequest(Method.GET, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<ServiceViewModel>(response.Content);

            return PartialView(model);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            try
            {
                SetParameter(new Parameter("id", id, ParameterType.UrlSegment));

                var response = SendRequest(Method.DELETE, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

                return ValidateResponse(response);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, errors = ex }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Details(long id)
        {
            SetParameter(new Parameter("id", id, ParameterType.UrlSegment));

            var response = SendRequest(Method.GET, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<ServiceViewModel>(response.Content);

            return PartialView(model);
        }

        public ActionResult Form(long id = 0)
        {
            if (id == 0)
            {
                ViewBag.ModalTitle = "Agregar servicio";
                ViewBag.SaveTextButton = "Guardar";
                return PartialView(new ServiceViewModel());
            }

            SetParameter(new Parameter("id", id, ParameterType.UrlSegment));

            var response = SendRequest(Method.GET, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<ServiceViewModel>(response.Content);

            ViewBag.ModalTitle = "Editar servicio";
            ViewBag.SaveTextButton = "Actualizar";
            return PartialView(model);
        }

        // GET: Services
        public ActionResult Index()
        {
            if (!SessionActive())
            {
                return Redirect("/Home/Login");
            }

            return View();
        }

        public ActionResult List()
        {
            Enums.ModelStatus status = Enums.ModelStatus.Active;
            if (Request.QueryString["Status"] != null)
            {
                status = (Enums.ModelStatus)int.Parse(Request.QueryString["Status"].ToString());
            }

            SetParameter(new Parameter("Status", status, ParameterType.QueryString));

            if (Request.QueryString["Name"] != null)
            {
                var name = Request.QueryString["Name"].ToString();

                SetParameter(new Parameter("Name", name, ParameterType.QueryString));
            }

            if (Request.QueryString["TypeServiceId"] != null)
            {
                var TypeServiceId = int.Parse(Request.QueryString["TypeServiceId"].ToString());

                SetParameter(new Parameter("TypeServiceId", TypeServiceId, ParameterType.QueryString));
            }

            var response = SendRequest(Method.GET, EndPoint, Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<IEnumerable<ServiceViewModel>>(response.Content);

            return PartialView(model);
        }

        [HttpPost]
        public JsonResult Save(Service model)
        {
            try
            {
                if (model.Id != 0)
                {
                    SetParameter(new Parameter("id", model.Id, ParameterType.UrlSegment));

                    SetBody(model);

                    var response = SendRequest(Method.PUT, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

                    return ValidateResponse(response);

                }
                else
                {
                    SetBody(model);

                    var response = SendRequest(Method.POST, EndPoint, Enums.Authentication.DEFAULT);

                    return ValidateResponse(response);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, errors = ex }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}