﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace tuconciergeweb.Controllers
{
    public class TypeAccountController : Controller
    {
        public ActionResult root()
        {
            return PartialView();
        }

        public ActionResult admin()
        {
            return PartialView();
        }

        public ActionResult responsable()
        {
            return PartialView();
        }
    }
}