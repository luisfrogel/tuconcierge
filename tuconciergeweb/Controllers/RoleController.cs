﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tuconciergemodels;
using tuconciergeweb.Interfaces;

namespace tuconciergeweb.Controllers
{
    public class RoleController : BaseController
    {
        private readonly string EndPoint = "/api/Roles";

        public RoleController() : base() { }

        public ActionResult ConfirmDelete(string id)
        {
            SetParameter(new Parameter("id", id, ParameterType.UrlSegment));

            var response = SendRequest(Method.GET, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<Role>(response.Content);

            return PartialView(model);
        }

        public JsonResult Delete(string id)
        {
            try
            {
                SetParameter(new Parameter("id", id, ParameterType.UrlSegment));

                var response = SendRequest(Method.DELETE, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

                return ValidateResponse(response);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, errors = ex }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Details(string id)
        {
            SetParameter(new Parameter("id", id, ParameterType.UrlSegment));

            var response = SendRequest(Method.GET, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<Role>(response.Content);

            return PartialView(model);
        }

        public ActionResult Form(string id = "")
        {
            if (id == "")
            {
                ViewBag.ModalTitle = "Agrega perfil";
                ViewBag.SaveTextButton = "Guardar";
                return PartialView(new Role());
            }

            SetParameter(new Parameter("id", id, ParameterType.UrlSegment));

            var response = SendRequest(Method.GET, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<Role>(response.Content);

            ViewBag.ModalTitle = "Editar perfil";
            ViewBag.SaveTextButton = "Actualizar";
            return PartialView(model);
        }

        public ActionResult Index()
        {
            if (!SessionActive())
            {
                return Redirect("/Home/Login");
            }

            return View();
        }

        public ActionResult List()
        {
            Enums.ModelStatus status = Enums.ModelStatus.Active;
            if (Request.QueryString["Status"] != null)
            {
                status = (Enums.ModelStatus)int.Parse(Request.QueryString["Status"].ToString());
            }

            SetParameter(new Parameter("Status", status, ParameterType.QueryString));

            if (Request.QueryString["Name"] != null)
            {
                var name = Request.QueryString["Name"].ToString();

                SetParameter(new Parameter("Name", name, ParameterType.QueryString));
            }

            var response = SendRequest(Method.GET, EndPoint, Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<IEnumerable<Role>>(response.Content);

            return PartialView(model);
        }

        public JsonResult Save(Role model)
        {
            try
            {
                if (model.Id != null)
                {
                    SetParameter(new Parameter("id", model.Id, ParameterType.UrlSegment));

                    SetBody(model);

                    var response = SendRequest(Method.PUT, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

                    return ValidateResponse(response);

                }
                else
                {
                    SetBody(model);

                    var response = SendRequest(Method.POST, EndPoint, Enums.Authentication.DEFAULT);

                    return ValidateResponse(response);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, errors = ex }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}