﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tuconciergemodels;
using tuconciergeweb.Interfaces;

namespace tuconciergeweb.Controllers
{
    public class ContractsController : BaseController
    {
        private readonly string EndPoint = "/api/Contracts";

        public ContractsController() : base() { }

        public ActionResult Details(long id)
        {
            SetParameter(new Parameter("id", id, ParameterType.UrlSegment));

            var response = SendRequest(Method.GET, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<Contract>(response.Content);

            return PartialView(model);
        }

        public ActionResult Form(long id = 0)
        {
            throw new NotImplementedException();
        }

        // GET: Contracts
        public ActionResult Index()
        {
            if (!SessionActive())
            {
                return Redirect("/Home/Login");
            }

            return View();
        }

        public ActionResult List()
        {
            Enums.ModelStatus status = Enums.ModelStatus.Active;
            if (Request.QueryString["Status"] != null)
            {
                status = (Enums.ModelStatus)int.Parse(Request.QueryString["Status"].ToString());
            }

            SetParameter(new Parameter("Status", status, ParameterType.QueryString));


            if (Request.QueryString["OfficeId"] != null)
            {
                var OfficeId = int.Parse(Request.QueryString["OfficeId"].ToString());

                SetParameter(new Parameter("OfficeId", OfficeId, ParameterType.QueryString));
            }

            if (Request.QueryString["AttentionStatus"] != null)
            {
                var AttentionStatus = int.Parse(Request.QueryString["AttentionStatus"].ToString());

                SetParameter(new Parameter("AttentionStatus", AttentionStatus, ParameterType.QueryString));
            }

           var response = SendRequest(Method.GET, EndPoint, Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<IEnumerable<Contract>>(response.Content);

            return PartialView(model);
        }
    }
}