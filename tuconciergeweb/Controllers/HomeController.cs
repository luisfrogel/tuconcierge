﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Mvc;
using tuconciergemodels;
using Newtonsoft.Json;

namespace tuconciergeweb.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Login(LoginUser model)
        {
            string EndPoint = "/Token";

            try
            {
                SetHeader("Content-Type", "application/x-www-form-urlencoded");

                SetParameter(new Parameter("grant_type", "password", ParameterType.GetOrPost));
                SetParameter(new Parameter("username", model.Username, ParameterType.GetOrPost));
                SetParameter(new Parameter("password", model.Password, ParameterType.GetOrPost));

                var response = SendRequest(Method.POST, EndPoint, Enums.Authentication.NONE);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    dynamic data = JsonConvert.DeserializeObject<dynamic>(response.Content);

                    Session["access_token"] = data.access_token;
                    Session["token_type"] = data.token_type;
                    Session["userName"] = data.userName;
                    Session["roleId"] = data.roleId;

                    return Json(new IResponseActions
                    {
                        Success = true,
                        Title = "Operación exitosa",
                        Message = "Espere un momento, será redirigido.",
                        TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt")
                    }, JsonRequestBehavior.AllowGet);
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    dynamic data = JsonConvert.DeserializeObject<dynamic>(response.Content);

                    return Json(new IResponseActions
                    {
                        Success = false,
                        Title = "Error al iniciar sesión",
                        Message = data.error_description,
                        TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt")
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new IResponseActions
                    {
                        Success = false,
                        Title = "Acceso denegado",
                        Message = "Inicie sesión para continuar.",
                        TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt")
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, errors = ex }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult Logout()
        {
            ClearSession();

            return Json(new IResponseActions
            {
                Success = true,
                TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt")
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            if (!SessionActive())
            {
                return Redirect("/Home/Login");
            }

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}