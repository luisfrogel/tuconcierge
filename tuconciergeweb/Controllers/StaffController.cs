﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tuconciergemodels;
using tuconciergeweb.Interfaces;
using tuconciergeweb.Services;

namespace tuconciergeweb.Controllers
{
    public class StaffController : BaseController
    {

        private readonly string EndPoint = "api/Staffs";

        public StaffController() : base() { }

        public ActionResult ConfirmDelete(long id)
        {
            SetParameter(new Parameter("id", id, ParameterType.UrlSegment));

            var response = SendRequest(Method.GET, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<StaffViewModel>(response.Content);

            return PartialView(model);
        }

        public JsonResult Delete(long id)
        {
            try
            {
                SetParameter(new Parameter("id", id, ParameterType.UrlSegment));

                var response = SendRequest(Method.DELETE, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

                return ValidateResponse(response);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, errors = ex }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Details(long id)
        {
            SetParameter(new Parameter("id", id, ParameterType.UrlSegment));

            var response = SendRequest(Method.GET, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<StaffViewModel>(response.Content);

            return PartialView(model);
        }

        public ActionResult Form(long id = 0)
        {
            if (id == 0)
            {
                ViewBag.ModalTitle = "Agregar personal";
                ViewBag.SaveTextButton = "Guardar";
                return PartialView(new StaffViewModel());
            }

            SetParameter(new Parameter("id", id, ParameterType.UrlSegment));

            var response = SendRequest(Method.GET, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<StaffViewModel>(response.Content);

            ViewBag.ModalTitle = "Editar personal";
            ViewBag.SaveTextButton = "Actualizar";
            return PartialView(model);
        }

        // GET: Staff
        public ActionResult Index()
        {
            if (!SessionActive())
            {
                return Redirect("/Home/Login");
            }

            return View();
        }

        public ActionResult List()
        {
            Enums.ModelStatus status = Enums.ModelStatus.Active;
            if (Request.QueryString["Status"] != null)
            {
                status = (Enums.ModelStatus)int.Parse(Request.QueryString["Status"].ToString());
            }

            SetParameter(new Parameter("Status", status, ParameterType.QueryString));

            if (Request.QueryString["Name"] != null)
            {
                var name = Request.QueryString["Name"].ToString();

                SetParameter(new Parameter("Name", name, ParameterType.QueryString));
            }

            if (Request.QueryString["FatherLastName"] != null)
            {
                var fatherLastName = Request.QueryString["FatherLastName"].ToString();

                SetParameter(new Parameter("FatherLastName", fatherLastName, ParameterType.QueryString));
            }

            if (Request.QueryString["OfficeId"] != null)
            {
                var OfficeId = Request.QueryString["OfficeId"].ToString();

                SetParameter(new Parameter("OfficeId", OfficeId, ParameterType.QueryString));
            }

            var response = SendRequest(Method.GET, EndPoint, Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<IEnumerable<StaffViewModel>>(response.Content);

            return PartialView(model);
        }

        public JsonResult Save(HttpPostedFileBase file, Staff model)
        {
            try
            {
                if (file != null)
                {
                    var fileUpload = new FileUploadService();

                    model.Photo = fileUpload.ImageUpload(file);
                }

                if (model.Id != 0)
                {
                    SetParameter(new Parameter("id", model.Id, ParameterType.UrlSegment));

                    SetBody(model);

                    var response = SendRequest(Method.PUT, EndPoint + "/{id}", Enums.Authentication.DEFAULT);

                    return ValidateResponse(response);

                }
                else
                {
                    SetBody(model);

                    var response = SendRequest(Method.POST, EndPoint, Enums.Authentication.DEFAULT);

                    return ValidateResponse(response);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, errors = ex }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Select()
        {
            var response = SendRequest(Method.GET, EndPoint, Enums.Authentication.DEFAULT);

            var model = JsonConvert.DeserializeObject<List<Staff>>(response.Content);

            return PartialView(model);
        }
    }
}