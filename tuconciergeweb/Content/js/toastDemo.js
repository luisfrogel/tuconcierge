(function($) {
  showSuccessToast = function(Title, Message) {
    'use strict';
    resetToastPosition();
      $.toast({
          heading: Title,
          text: Message,
          showHideTransition: 'slide',
          icon: 'success',
          loaderBg: '#f96868',
          position: 'top-right'
      });
  };
    showInfoToast = function (Title, Message) {
    'use strict';
    resetToastPosition();
      $.toast({
          heading: Title,
          text: Message,
          showHideTransition: 'slide',
          icon: 'info',
          loaderBg: '#46c35f',
          position: 'top-right',
          hideAfter: 5000
      });
  };
  showWarningToast = function(Title, Message) {
    'use strict';
    resetToastPosition();
      $.toast({
          heading: Title,
          text: Message,
          showHideTransition: 'slide',
          icon: 'warning',
          loaderBg: '#57c7d4',
          position: 'top-right',
          hideAfter: 5000
      });
  };
  showDangerToast = function(Title, Message) {
    'use strict';
    resetToastPosition();
      $.toast({
          heading: Title,
          text: Message,
          showHideTransition: 'slide',
          icon: 'error',
          loaderBg: '#f2a654',
          position: 'top-right',
          hideAfter: 5000
      });
  };
    showToastPosition = function (position) {
        'use strict';
        resetToastPosition();
        $.toast({
            heading: 'Positioning',
            text: 'Specify the custom position object or use one of the predefined ones',
            position: String(position),
            icon: 'info',
            stack: false,
            loaderBg: '#f96868'
        });
    };
    showToastInCustomPosition = function () {
        'use strict';
        resetToastPosition();
        $.toast({
            heading: 'Custom positioning',
            text: 'Specify the custom position object or use one of the predefined ones',
            icon: 'info',
            position: {
                left: 120,
                top: 120
            },
            stack: false,
            loaderBg: '#f96868'
        });
    };
    resetToastPosition = function () {
        $('.jq-toast-wrap').removeClass('bottom-left bottom-right top-left top-right mid-center'); // to remove previous position class
        $(".jq-toast-wrap").css({
            "top": "",
            "left": "",
            "bottom": "",
            "right": ""
        }); //to remove previous position style
    };
})(jQuery);