

function initialize() {

    var mapOptions, map, marker, searchBox, city,
        infoWindow = '',
        addressEl = document.querySelector('#map-search'),
        latEl = document.querySelector('.latitude'),
        longEl = document.querySelector('.longitude'),
        element = document.getElementById('map-canvas');
    city = document.querySelector('.reg-input-city');

    var formLat = document.getElementById('Lat').value;
    var formLong = document.getElementById('Long').value;
    var formRatio = document.getElementById('Ratio').value;

    if (formLat === "" || formLat == null) {
        formLat = 20.962744;
    }

    if (formLong === "" || formLong == null) {
        formLong = -89.626822;
    }

    if (formRatio === "" || formRatio == null) {
        formRatio = 500;
    }

    mapOptions = {
        zoom: 14,
        center: new google.maps.LatLng(formLat, formLong),
        disableDefaultUI: false, // Disables the controls like zoom control on the map if set to true
        scrollWheel: true, // If set to false disables the scrolling on the map.
        draggable: true// If set to false , you cannot move the map around.
        // mapTypeId: google.maps.MapTypeId.HYBRID, // If set to HYBRID its between sat and ROADMAP, Can be set to SATELLITE as well.
        // maxZoom: 11, // Wont allow you to zoom more than this
        // minZoom: 9  // Wont allow you to go more up.

    };

	/**
	 * Creates the map using google function google.maps.Map() by passing the id of canvas and
	 * mapOptions object that we just created above as its parameters.
	 *
	 */
    // Create an object map with the constructor function Map()
    map = new google.maps.Map(element, mapOptions); // Till this like of code it loads up the map.

    var c = {
        strokeColor: "#ff0000",
        strokeOpacity: 0.8,
        strokeWeight: 3,
        fillColor: "#b0c4de",
        fillOpacity: 0.50,
        map: map,
        center: new google.maps.LatLng(formLat, formLong),
        radius: Number(formRatio),
        editable: true
    };

    circle = new google.maps.Circle(c);

    google.maps.event.addListener(circle, 'center_changed', function () {
        //console.log('lat', circle.getCenter().lat());
        //console.log('lng', circle.getCenter().lng());
        document.getElementById('Lat').value = circle.getCenter().lat();
        document.getElementById('Long').value = circle.getCenter().lng();
    });

    google.maps.event.addListener(circle, 'radius_changed', function () {
        //console.log('radius', circle.getRadius());
        document.getElementById('Ratio').value = circle.getRadius();
    }); 

	/**
	 * Creates a search box
	 */
    searchBox = new google.maps.places.SearchBox(addressEl);

	/**
	 * When the place is changed on search box, it takes the marker to the searched location.
	 */
    google.maps.event.addListener(searchBox, 'places_changed', function () {
        var places = searchBox.getPlaces(),
            bounds = new google.maps.LatLngBounds(),
            i, place, lat, long, resultArray,
            addresss = places[0].formatted_address;

        for (i = 0; place = places[i]; i++) {
            bounds.extend(place.geometry.location);
            //marker.setPosition(place.geometry.location);  // Set marker position new.
            circle.setCenter(place.geometry.location);
        }

        map.fitBounds(bounds);  // Fit to the bound
        map.setZoom(14); // This function sets the zoom to 15, meaning zooms to level 15.
        // console.log( map.getZoom() );

        lat = circle.getCenter().lat();
        long = circle.getCenter().lng();
        latEl.value = lat;
        longEl.value = long;

        resultArray = places[0].address_components;

        // Closes the previous info window if it already exists
        if (infoWindow) {
            infoWindow.close();
        }
		/**
		 * Creates the info Window at the top of the marker
		 */
        infoWindow = new google.maps.InfoWindow({
            content: addresss
        });

        //infoWindow.open(map, circle);

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                infoWindow.setPosition(pos);
                infoWindow.setContent('Location found.');
                infoWindow.open(map, circle);
                map.setCenter(pos);
            }, function () {
                handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map, circle);
        }


    }); 

}