﻿using System.Web;
using System.Web.Optimization;

namespace tuconciergeweb
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            // Es utilizado para pluggins
            bundles.Add(new ScriptBundle("~/Bundle/js/pluggins").Include(
                "~/Content/vendors/jquery-validation/jquery.validate.min.js",
                "~/Content/vendors/bootstrap-maxlength/bootstrap-maxlength.min.js",
                "~/Content/vendors/sweetalert/sweetalert.min.js",
                "~/Content/vendors/jquery.avgrund/jquery.avgrund.min.js",
                "~/Content/vendors/chart.js/Chart.min.js",
                "~/Content/vendors/owl-carousel-2/owl.carousel.min.js",
                "~/Content/vendors/jvectormap/jquery-jvectormap.min.js",
                "~/Content/vendors/jquery-bar-rating/jquery.barrating.min.js",
                "~/Content/vendors/jquery-asColor/jquery-asColor.min.js",
                "~/Content/vendors/jquery-asGradient/jquery-asGradient.min.js",
                "~/Content/vendors/jquery-asColorPicker/jquery-asColorPicker.min.js",
                "~/Content/vendors/x-editable/bootstrap-editable.min.js",
                "~/Content/vendors/moment/moment.min.js",
                "~/Content/vendors/tempusdominus-bootstrap-4/tempusdominus-bootstrap-4.js",
                "~/Content/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js",
                "~/Content/vendors/dropify/dropify.min.js",
                "~/Content/vendors/jquery-file-upload/jquery.uploadfile.min.js",
                "~/Content/vendors/jquery-tags-input/jquery.tagsinput.min.js",
                "~/Content/vendors/jquery-toast-plugin/jquery.toast.min.js",
                "~/Content/vendors/dropzone/dropzone.js",
                "~/Content/vendors/jquery.repeater/jquery.repeater.min.js",
                "~/Content/vendors/inputmask/jquery.inputmask.bundle.js",
                "~/Content/vendors/inputmask/inputmask.binding.js",
                "~/Content/vendors/jvectormap/jquery-jvectormap-world-mill-en.js"));

            // Es utilizado
            bundles.Add(new ScriptBundle("~/Bundle/js").Include(
                "~/Content/js/modal-demo.js",
                "~/Content/js/form-validation.js",
                "~/Content/js/bt-maxLength.js",
                "~/Content/js/alerts.js",
                "~/Content/js/avgrund.js",
                "~/Content/js/jq.tablesort.js",
                "~/Content/js/tablesorter.js",
                "~/Content/js/formpickers.js",
                "~/Content/js/form-addons.js",
                "~/Content/js/x-editable.js",
                "~/Content/js/dropify.js",
                "~/Content/js/dropzone.js",
                "~/Content/js/jquery-file-upload.js",
                "~/Content/js/formpickers.js",
                "~/Content/js/form-repeater.js",
                "~/Content/js/toastDemo.js",
                "~/Content/js/dashboard.js"));

            bundles.Add(new ScriptBundle("~/Bundle/js/essential").Include(
                 "~/Content/vendors/js/vendor.bundle.base.js",
                "~/Content/js/off-canvas.js",
                "~/Content/js/hoverable-collapse.js",
                "~/Content/js/template.js",
                "~/Content/js/settings.js",
                "~/Content/js/todolist.js"
                ));

            // Es utiizado
            bundles.Add(new StyleBundle("~/Bundle/css").Include(
                      "~/Content/vendors/ionicons/css/ionicons.min.css",
                      "~/Content/vendors/css/vendor.bundle.base.css",
                      "~/Content/vendors/owl-carousel-2/owl.carousel.min.css",
                      "~/Content/vendors/owl-carousel-2/owl.theme.default.min.css",
                      "~/Content/vendors/select2/select2.min.css",
                      "~/Content/vendors/select2-bootstrap-theme/select2-bootstrap.min.css",
                      "~/Content/vendors/font-awesome/css/font-awesome.min.css",
                      "~/Content/vendors/jquery-bar-rating/bars-1to10.css",
                      "~/Content/vendors/jquery-bar-rating/bars-horizontal.css",
                      "~/Content/vendors/jquery-bar-rating/bars-movie.css",
                      "~/Content/vendors/jquery-bar-rating/bars-pill.css",
                      "~/Content/vendors/jquery-bar-rating/bars-reversed.css",
                      "~/Content/vendors/jquery-bar-rating/bars-square.css",
                      "~/Content/vendors/jquery-bar-rating/bootstrap-stars.css",
                      "~/Content/vendors/jquery-bar-rating/css-stars.css",
                      "~/Content/vendors/jquery-bar-rating/examples.css",
                      "~/Content/vendors/jquery-bar-rating/fontawesome-stars-o.css",
                      "~/Content/vendors/jquery-bar-rating/fontawesome-stars.css",
                      "~/Content/vendors/jquery-asColorPicker/css/asColorPicker.min.css",
                      "~/Content/vendors/x-editable/bootstrap-editable.css",
                      "~/Content/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css",
                      "~/Content/vendors/dropify/dropify.min.css",
                      "~/Content/vendors/jquery-file-upload/uploadfile.css",
                      "~/Content/vendors/jquery-toast-plugin/jquery.toast.min.css",
                      "~/Content/vendors/jquery-tags-input/jquery.tagsinput.min.css",
                      "~/Content/vendors/tempusdominus-bootstrap-4/tempusdominus-bootstrap-4.min.css",
                      "~/Content/vendors/dropzone/dropzone.css",
                      "~/Content/css/horizontal-layout-light/style.css"));

        }
    }
}
