﻿using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;

namespace tuconciergeweb.Services
{
    public class FileUploadService
    {

        private readonly Account account;
        private Cloudinary cloudinary;

        public FileUploadService()
        {
            account = new Account("lfrr", "762173975311971", "hd4CyGQb_BX478D7yq9ax5UdHkg");
            cloudinary = new Cloudinary(account);
        }

        public string ImageUpload(HttpPostedFileBase file)
        {
            string filename = string.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), file.FileName);

            var uploadParams = new ImageUploadParams()
            {
                File = new FileDescription(file.FileName, file.InputStream),
                UseFilename = true
            };
            var uploadResult = cloudinary.Upload(uploadParams);

            return uploadResult.SecureUri.ToString();
        }

        public string ImageUpload(string base64String)
        {
            var file = Base64ToInputScreen(base64String);

            string filename = string.Format("{0}", DateTime.Now.ToString("yyyyMMddHHmmss"));

            var uploadParams = new ImageUploadParams()
            {
                File = new FileDescription(filename, file),
                UseFilename = true
            };
            var uploadResult = cloudinary.Upload(uploadParams);

            return uploadResult.SecureUri.ToString();
        }


        private MemoryStream Base64ToInputScreen(string base64String)
        {
            byte[] byteArray = Encoding.ASCII.GetBytes(base64String);
            MemoryStream stream = new MemoryStream(byteArray);

            return stream;
        }
    }
}