﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tuconciergemodels;

namespace tuconciergeweb.Interfaces
{
    public interface IViewController<T>
    {
        ActionResult Index();

        ActionResult List();

        ActionResult Form(long id = 0);

        [HttpPost]
        JsonResult Save(T model);

        ActionResult ConfirmDelete(long id);

        [HttpPost]
        JsonResult Delete(long id);

        ActionResult Details(long id);
    }
}