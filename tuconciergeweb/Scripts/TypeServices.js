﻿    $(document).ready(function () {

    $('.nav-item').removeClass('active');
    $('.nav-link').removeClass('active');
    $('#navTypeServices').addClass('active');

    getList();
});

function getList() {

    var url = '/TypeServices/List';
    var table = $('#MyTable');

    var name = $('#NameFilter').val();
    var filter = false;
    var params = '';

    if (name !== '') {
        params += 'Name=' + name + '&';
        filter = true;
    }

    if (filter) {
        url += '?' + params.slice(0, -1);
    }

    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            table.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            table.html(data);
        }
    });
}


function Create() {

    var MyModal = $('#mdModal');
    var modalContent = $('#mdModalContent');

    modalContent.html('');
    MyModal.modal('show');

    $.ajax({
        url: '/TypeServices/Form',
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
        }
    });
}


function Edit(id) {
    var MyModal = $('#mdModal');
    var modalContent = $('#mdModalContent');

    modalContent.html('');

    MyModal.modal('show');

    $.ajax({
        url: '/TypeServices/Form/' + id,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
        }
    });
}

function Save() {

    var name = $('#Name').val();
    var image = $('#Image').val();

    var ExistErrors = false;

    if (name === '') {
        showDangerToast('Error de validación', 'El campo Nombre no puede estar vacío.');
        ExistErrors = true;
    }

    var files = $("#files")[0].files;

    if (files.length === 0 && image === '') {
        showDangerToast('Error de validación', 'El campo Imagen no puede estar vacío.');
        ExistErrors = true;
    }

    if (ExistErrors) {
        return;
    }

    var form = $('#typeServiceForm')[0];
    var formData = new FormData(form);

    $.ajax({
        url: '/TypeServices/Save',
        type: 'POST',
        enctype: 'multipart/form-data',
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        beforeSend: function () {
            $('#btnSave').attr('disabled', true);
            $('#btnCancel').attr('disabled', true);
        },
        success: function (data) {
            console.log('Success');
            if (data.Success) {

                showSuccessToast(data.Title, data.Message);
                getList();
                $('#mdModal').modal('hide');
                $('#mdModalContent').html('');

            } else {

                showWarningToast(data.Title, data.Message);
                $('#btnSave').removeAttr('disabled');
                $('#btnCancel').removeAttr('disabled');
            }
        },
        error: function (data) {
            console.log('Errores');
            console.log(data);

            showDangerToast(data.Title, data.Message);
        }
    });
}

function ConfirmDelete(id) {
    var MyModal = $('#smModal');
    var modalContent = $('#smModalContent');

    modalContent.html('');

    MyModal.modal('show');

    $.ajax({
        url: '/TypeServices/ConfirmDelete/' + id,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
        }
    });
}


function Delete(id) {
    $.ajax({
        url: '/TypeServices/Delete/' + id,
        type: 'POST',
        dataType: 'json',
        beforeSend: function () {
            $('#btnDelete').attr('disabled', true);
            $('#btnCancel').attr('disabled', true);
        },
        success: function (data) {
            console.log('Success');
            if (data.Success) {

                showSuccessToast(data.Title, data.Message);
                getList();
                $('#smModal').modal('hide');
                $('#smModalContent').html('');

            } else {

                showWarningToast(data.Title, data.Message);
                $('#btnDelete').removeAttr('disabled');
                $('#btnCancel').removeAttr('disabled');
            }
        },
        error: function (data) {
            console.log('Errores');
            console.log(data);

            showDangerToast(data.Title, data.Message);
        }
    });
}

function Details(id) {
    var MyModal = $('#mdModal');
    var modalContent = $('#mdModalContent');

    modalContent.html('');

    MyModal.modal('show');

    $.ajax({
        url: '/TypeServices/Details/' + id,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
        }
    });
}