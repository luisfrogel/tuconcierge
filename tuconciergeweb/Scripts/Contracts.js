﻿$(document).ready(function () {

    $('.nav-item').removeClass('active');
    $('.nav-link').removeClass('active');
    $('#navContracts').addClass('active');

    getList();
});

function getList() {

    var url = '/Contracts/List';
    var table = $('#MyTable');

    var officeId = $('#selectOfficeFilter').val();
    var attentionStatus = $('#selectAttentionStatus').val();
    var filter = false;
    var params = '';

    if (officeId !== 0) {
        params += 'OfficeId=' + officeId + '&';
        filter = true;
    }

    if (attentionStatus !== 0) {
        params += 'AttentionStatus=' + attentionStatus + '&';
        filter = true;
    }

    if (filter) {
        url += '?' + params.slice(0, -1);
    }

    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            table.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            table.html(data);
        }
    });
}