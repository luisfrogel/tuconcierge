﻿
function getListComp(id) {
    var url = '/Complements/List';
    var table = $('#mdModalContent');

    var idSalesplanId = id;

    var name = $('#NameFilter').val();

    var filter = false;
    var params = '';

    if (idSalesplanId !== 0) {
        params += 'SalesPlanId=' + idSalesplanId + '&';
        filter = true;
    }

    if (name !== '') {
        params += 'Name=' + name + '&';
        filter = true;
    }

    if (filter) {
        url += '?' + params.slice(0, -1);
    }

    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            table.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            table.html(data);
            $('#SalesPlanID').val(idSalesplanId);
        }
    });
}


function createComp() {

    var MyModal = $('#smModal');
    var modalContent = $('#smModalContent');

    var salesPlanID = $('#SalesPlanID').val();

    modalContent.html('');
    MyModal.modal('show');

    $.ajax({
        url: '/Complements/Form?SalesPlanId=' + salesPlanID,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
            $('#salesPlanID').val(salesPlanID);
        }
    });
}

function SaveComp() {
    var name = $('#Name').val();
    var precio = $('#Cost').val();
    var SalesPlanid = $('#salesPlanID').val();


    var ExistErrors = false;

    if (name === '') {
        showDangerToast('Error de validación', 'El campo Nombre no puede estar vacío.');
        ExistErrors = true;
    }

    if (precio === '') {
        showDangerToast('Error de validación', 'El campo precio no puede estar vacío.');
        ExistErrors = true;
    }


    if (ExistErrors) {
        return;
    }

    var data = {
        Id: Number($('#Id').val()),
        Name: name,
        Cost: precio,
        SalesPlanId: SalesPlanid
    };
    console.log(data);
    $.ajax({
        url: '/Complements/Save',
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function () {
            $('#btnSave').attr('disabled', true);
            $('#btnCancel').attr('disabled', true);
        },
        success: function (data) {
            console.log('Success');
            if (data.Success) {

                showSuccessToast(data.Title, data.Message);
                getListComp(SalesPlanid);
                $('#smModal').modal('hide');
                $('#smModalContent').html('');
            } else {

                showWarningToast(data.Title, data.Message);
                $('#btnSave').removeAttr('disabled');
                $('#btnCancel').removeAttr('disabled');
            }
        },
        error: function (data) {
            console.log('Errores');
            console.log(data);

            showDangerToast(data.Title, data.Message);
        }
    });
}


function ConfirmDeleteComp(id) {
    var MyModal = $('#smModal');
    var modalContent = $('#smModalContent');
    var idSP = $('#SalesPlanID').val();
    modalContent.html('');

    MyModal.modal('show');

    $.ajax({
        url: '/Complements/ConfirmDelete/' + id,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
            $('#salesplanID').val(idSP);
        }
    });
}

function DeleteSP(id) {
    var idSp = $('#salesplanID').val();
    $.ajax({
        url: '/Complements/Delete/' + id,
        type: 'POST',
        dataType: 'json',
        beforeSend: function () {
            $('#btnDelete').attr('disabled', true);
            $('#btnCancel').attr('disabled', true);
        },
        success: function (data) {
            console.log('Success');
            if (data.Success) {

                showSuccessToast(data.Title, data.Message);
                getListComp(idSp);
                $('#smModal').modal('hide');
                $('#smModalContent').html('');

            } else {

                showWarningToast(data.Title, data.Message);
                $('#btnDelete').removeAttr('disabled');
                $('#btnCancel').removeAttr('disabled');
            }
        },
        error: function (data) {
            console.log('Errores');
            console.log(data);

            showDangerToast(data.Title, data.Message);
        }
    });
}

function EditComp(id) {
    var MyModal = $('#smModal');
    var modalContent = $('#smModalContent');

    modalContent.html('');

    MyModal.modal('show');

    var idSp = $('#SalesPlanID').val();

    $.ajax({
        url: '/Complements/Form/' + id,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
            $('#salesPlanID').val(idSp);
        }
    });
}