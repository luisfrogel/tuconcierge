﻿$(document).ready(function () {

    $('.nav-item').removeClass('active');
    $('#navCompanies').addClass('active');

    getList();
});

function getList() {

    var url = '/Companies/List';
    var table = $('#MyTable');

    var FiscalName = $('#FiscalNameFilter').val();
    var ComercialName = $('#ComercialNameFilter').val();
    var RFC = $('#RFCFilter').val();
    var filter = false;
    var params = '';

    if (FiscalName !== '') {
        params += 'FiscalName=' + FiscalName + '&';
        filter = true;
    }

    if (ComercialName !== '') {
        params += 'ComercialName=' + ComercialName + '&';
        filter = true;
    }

    if (RFC !== '') {
        params += 'RFC=' + RFC + '&';
        filter = true;
    }

    if (filter) {
        url += '?' + params.slice(0, -1);
    }

    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            table.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            table.html(data);
        }
    });
}

function Create() {

    var MyModal = $('#mdModal');
    var modalContent = $('#mdModalContent');

    modalContent.html('');
    MyModal.modal('show');

    $.ajax({
        url: '/Companies/Form',
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
        }
    });
}

function Edit(id) {
    var MyModal = $('#mdModal');
    var modalContent = $('#mdModalContent');

    modalContent.html('');

    MyModal.modal('show');

    $.ajax({
        url: '/Companies/Form/' + id,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
        }
    });
}

function Save() {
    var FiscalName = $('#FiscalName').val();
    var ComercialName = $('#ComercialName').val();
    var RFC = $('#RFC').val();
    var FiscalRegime = $('#FiscalRegime').val();
    var AdminName = $('#AdminName').val();
    var AdminLastName = $('#AdminLastName').val();
    var AdminEmail = $('#AdminEmail').val();
    var AdminPhoneNumber = $('#AdminPhoneNumber').val();

    var ExistErrors = false;

    if (FiscalName === '') {
        showDangerToast('Error de validación', 'El campo Nombre fiscal no puede estar vacío.');
        ExistErrors = true;
    }

    if (ComercialName === '') {
        showDangerToast('Error de validación', 'El campo Nombre comercial no puede estar vacío.');
        ExistErrors = true;
    }

    if (RFC === '') {
        showDangerToast('Error de validación', 'El campo Nombre comercial no puede estar vacío.');
        ExistErrors = true;
    }

    if (RFC.length < 12) {
        showDangerToast('Error de validación', 'El campo RFC no puede contener menos de 12 carácteres.');
        ExistErrors = true;
    }

    if (RFC.length > 13) {
        showDangerToast('Error de validación', 'El campo RFC no puede contener más de 13 carácteres.');
        ExistErrors = true;
    }

    if (FiscalRegime === '') {
        showDangerToast('Error de validación', 'El campo Régimen fiscall no puede estar vacío.');
        ExistErrors = true;
    }

    if (AdminName === '') {
        showDangerToast('Error de validación', 'El campo Nombre no puede estar vacío.');
        ExistErrors = true;
    }

    if (AdminLastName === '') {
        showDangerToast('Error de validación', 'El campo Apellidos no puede estar vacío.');
        ExistErrors = true;
    }

    if (AdminEmail === '') {
        showDangerToast('Error de validación', 'El campo Correo electrónico no puede estar vacío.');
        ExistErrors = true;
    }

    if (ExistErrors) {
        return;
    }

    var data = {
        Id: Number($('#Id').val()),
        FiscalName: FiscalName,
        ComercialName: ComercialName,
        RFC: RFC,
        FiscalRegime: FiscalRegime,
        AdminName: AdminName,
        AdminLastName: AdminLastName,
        AdminEmail: AdminEmail,
        AdminPhoneNumber: AdminPhoneNumber
    };

    $.ajax({
        url: '/Companies/Save',
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function () {
            $('#btnSave').attr('disabled', true);
            $('#btnCancel').attr('disabled', true);
        },
        success: function (data) {
            console.log('Success');
            if (data.Success) {

                showSuccessToast(data.Title, data.Message);
                getList();
                $('#mdModal').modal('hide');
                $('#mdModalContent').html('');

            } else {

                showWarningToast(data.Title, data.Message);
                $('#btnSave').removeAttr('disabled');
                $('#btnCancel').removeAttr('disabled');
            }
        },
        error: function (data) {
            console.log('Errores');
            console.log(data);

            showDangerToast(data.Title, data.Message);
        }
    });
}

function ConfirmDelete(id) {
    var MyModal = $('#smModal');
    var modalContent = $('#smModalContent');

    modalContent.html('');

    MyModal.modal('show');

    $.ajax({
        url: '/Companies/ConfirmDelete/' + id,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
        }
    });
}

function Delete(id) {
    $.ajax({
        url: '/Companies/Delete/' + id,
        type: 'POST',
        dataType: 'json',
        beforeSend: function () {
            $('#btnDelete').attr('disabled', true);
            $('#btnCancel').attr('disabled', true);
        },
        success: function (data) {
            console.log('Success');
            if (data.Success) {

                showSuccessToast(data.Title, data.Message);
                getList();
                $('#smModal').modal('hide');
                $('#smModalContent').html('');

            } else {

                showWarningToast(data.Title, data.Message);
                $('#btnDelete').removeAttr('disabled');
                $('#btnCancel').removeAttr('disabled');
            }
        },
        error: function (data) {
            console.log('Errores');
            console.log(data);

            showDangerToast(data.Title, data.Message);
        }
    });
}

function Details(id) {
    var MyModal = $('#mdModal');
    var modalContent = $('#mdModalContent');

    modalContent.html('');

    MyModal.modal('show');

    $.ajax({
        url: '/Companies/Details/' + id,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
        }
    });
}