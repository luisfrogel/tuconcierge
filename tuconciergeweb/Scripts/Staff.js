﻿$(document).ready(function () {

    $('.nav-item').removeClass('active');
    $('.nav-link').removeClass('active');
    $('#navStaff').addClass('active');

    getList();
});

function getList()
{
    var url = '/Staff/List';
    var table = $('#MyTable');

    var OfficeId = Number($('#selectOfficeFilter').val());

    var name = $('#NameFilter').val();

    var lastname = $('#LastNameFilter').val();

    var filter = false;
    var params = '';

    if (OfficeId !== 0) {
        params += 'OfficeId=' + OfficeId + '&';
        filter = true;
    }

    if (name !== '') {
        params += 'Name=' + name + '&';
        filter = true;
    }

    if (lastname !== '') {
        params += 'FatherLastName=' + lastname + '&';
        console.log(lastname);
        filter = true;
    }

    if (filter) {
        url += '?' + params.slice(0, -1);
    }

    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            table.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            table.html(data);
        }
    });
}
function Create() {

    var MyModal = $('#mdModal');
    var modalContent = $('#mdModalContent');

    modalContent.html('');
    MyModal.modal('show');

    $.ajax({
        url: '/Staff/Form',
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
        }
    });
}

function Edit(id) {
    var MyModal = $('#mdModal');
    var modalContent = $('#mdModalContent');

    modalContent.html('');

    MyModal.modal('show');

    $.ajax({
        url: '/Staff/Form/' + id,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: async function (data) {
            modalContent.html(data);

            $('#selectOffice').val($('#OfficeId').val());

            $('#selectCountry').val($('#CountryId').val());

            await cargarStates($('#CountryId').val());

            $('#selectStates').val($('#StateId').val());
        }
    });
}

function Save() {
    var name = $('#Name').val();
    var fatherLastName = $('#FatherLastName').val();
    var motherLastName = $('#MotherLastName').val();
    var birth = $('#Birth').val();
    var gender = $('#Gender').val();
    var maritalstate = $('#MaritalState').val();
    var curp = $('#CURP').val();
    var rfc = $('#RFC').val();
    var email = $('#Email').val();
    var phoneNumber = $('#PhoneNumber').val();
    var address = $('#Address').val();
    var city = $('#City').val();
    var selectOffice = Number($('#selectOffice').val());
    var selectState = Number($('#selectStates').val());


    var ExistErrors = false;

    var image = $('#Photo').val();
    var files = $("#files")[0].files;

    if (files.length === 0 && image === '') {
        showDangerToast('Error de validación', 'El campo Imagen no puede estar vacío.');
        ExistErrors = true;
    }

    if (name === '') {
        showDangerToast('Error de validación', 'El campo Nombre no puede estar vacío.');
        ExistErrors = true;
    }

    if (fatherLastName == '') {
        showDangerToast('Error de validación', 'El campo Apellido paterno no puede estar vacío');
        ExistErrors = true;
    }

    if (motherLastName == '') {
        showDangerToast('Error de validación', 'El campo Apellido materno no puede estar vacío');
        ExistErrors = true;
    }

    if (birth == '') {
        showDangerToast('Error de validación', 'El campo Fecha de nacimiento no puede estar vacío');
        ExistErrors = true;
    }

    if (curp == '') {
        showDangerToast('Error de validación', 'El campo CURP no puede estar vacío');
        ExistErrors = true;
    }

    if (phoneNumber == '') {
        showDangerToast('Error de validación', 'El campo Número de teléfono no puede estar vacío');
        ExistErrors = true;
    }

    if (selectOffice == 0) {
        showDangerToast('Error de validación', 'Debe seleccionar un dato del campo Oficina.');
        ExistErrors = true;
    }

    if (ExistErrors) {
        return;
    }

    $('#OfficeId').val(selectOffice);
    $('#StateId').val(selectState);

    var form = $('#staffForm')[0];
    var formData = new FormData(form);

    $.ajax({
        url: '/Staff/Save',
        type: 'POST',
        enctype: 'multipart/form-data',
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        beforeSend: function () {
            $('#btnSave').attr('disabled', true);
            $('#btnCancel').attr('disabled', true);
        },
        success: function (data) {
            console.log('Success');
            if (data.Success) {

                showSuccessToast(data.Title, data.Message);
                getList();
                $('#mdModal').modal('hide');
                $('#mdModalContent').html('');

            } else {

                showWarningToast(data.Title, data.Message);
                $('#btnSave').removeAttr('disabled');
                $('#btnCancel').removeAttr('disabled');
            }
        },
        error: function (data) {
            console.log('Errores');
            console.log(data);

            showDangerToast(data.Title, data.Message);
        }
    });
}

function ConfirmDelete(id) {
    var MyModal = $('#smModal');
    var modalContent = $('#smModalContent');

    modalContent.html('');

    MyModal.modal('show');

    $.ajax({
        url: '/Staff/ConfirmDelete/' + id,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
        }
    });
}

function Delete(id) {
    $.ajax({
        url: '/Staff/Delete/' + id,
        type: 'POST',
        dataType: 'json',
        beforeSend: function () {
            $('#btnDelete').attr('disabled', true);
            $('#btnCancel').attr('disabled', true);
        },
        success: function (data) {
            console.log('Success');
            if (data.Success) {

                showSuccessToast(data.Title, data.Message);
                getList();
                $('#smModal').modal('hide');
                $('#smModalContent').html('');

            } else {

                showWarningToast(data.Title, data.Message);
                $('#btnDelete').removeAttr('disabled');
                $('#btnCancel').removeAttr('disabled');
            }
        },
        error: function (data) {
            console.log('Errores');
            console.log(data);

            showDangerToast(data.Title, data.Message);
        }
    });
}

function Details(id) {
    var MyModal = $('#mdModal');
    var modalContent = $('#mdModalContent');

    modalContent.html('');

    MyModal.modal('show');

    $.ajax({
        url: '/Staff/Details/' + id,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
        }
    });
}

async function getStates(self) {

    await cargarStates(self.value);
}

async function cargarStates(id) {

    var selectStates = $('#selectStates');

    await $.ajax({
        url: '/States/Select?CountryId=' + id,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            $('#selectStates').attr("disabled");
        },
        success: function (data) {
            selectStates.html(data);
            selectStates.removeAttr("disabled");
        }
    });
}