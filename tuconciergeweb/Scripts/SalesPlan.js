﻿

function getListTab() {
    var url = '/SalesPlan/List';
    var table = $('#listTab');

    var idService = $('#ServiceID').val();

    var name = $('#NameFilter').val();

    var filter = false;
    var params = '';

    if (idService !== 0) {
        params += 'ServiceId=' + idService + '&';
        filter = true;
    }

    if (name !== '') {
        params += 'Name=' + name + '&';
        filter = true;
    }

    if (filter) {
        url += '?' + params.slice(0, -1);
    }

    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            table.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            table.html(data);
            $('#ServiceIdC').val(idService);
        }
    });
}

function CreateSalesP() {

    $('#formASP').removeAttr('hidden');

}

function SaveEdSP() {
    var name = $('#name').val();
    var precio = $('#precio').val();
    var servicesID = $('#ServiceIdC').val();
    var descripcion = $('#descripcion').val();

    var ExistErrors = false;

    if (name === '') {
        showDangerToast('Error de validación', 'El campo Nombre no puede estar vacío.');
        ExistErrors = true;
    }

    if (precio === '') {
        showDangerToast('Error de validación', 'El campo precio no puede estar vacío.');
        ExistErrors = true;
    }

    if (descripcion === '') {
        showDangerToast('Error de validación', 'El campo descripcion no puede estar vacío.');
        ExistErrors = true;
    }


    if (ExistErrors) {
        return;
    }

    var data = {
        Id: Number($('#Id').val()),
        Name: name,
        Cost: precio,
        Description: descripcion,
        ServicesId: servicesID
    };
    console.log(data);
    $.ajax({
        url: '/SalesPlan/Save',
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function () {
            $('#btnSave').attr('disabled', true);
            $('#btnCancel').attr('disabled', true);
        },
        success: function (data) {
            console.log('Success');
            if (data.Success) {

                showSuccessToast(data.Title, data.Message);
                getListTab();

            } else {

                showWarningToast(data.Title, data.Message);
                $('#btnSave').removeAttr('disabled');
                $('#btnCancel').removeAttr('disabled');
            }
        },
        error: function (data) {
            console.log('Errores');
            console.log(data);

            showDangerToast(data.Title, data.Message);
        }
    });
}

function saveSalesP() {

    var name = $('#name').val();
    var precio = $('#precio').val();
    var servicesId = $('#ServiceIdC').val();
    var descripcion = $('#descripcion').val();

    var ExistErrors = false;

    if (name === '') {
        showDangerToast('Error de validación', 'El campo Nombre no puede estar vacío.');
        ExistErrors = true;
    }

    if (precio === '') {
        showDangerToast('Error de validación', 'El campo precio no puede estar vacío.');
        ExistErrors = true;
    }

     if (descripcion === '') {
        showDangerToast('Error de validación', 'El campo descripcion no puede estar vacío.');
        ExistErrors = true;
    }


    if (ExistErrors) {
        return;
    }

    var data = {
        'Name': name,
        'Cost': precio,
        'Description': descripcion,
        'ServicesId': servicesId
    }

    $.ajax({
        url: '/SalesPlan/Save',
        type: 'POST',
        dataType: 'JSON',
        data: data,
        beforeSend: function () {
            $('#btnSave').attr('disabled', true);
            $('#btnCancel').attr('disabled', true);
        },
        success: function (data) {
            console.log('Success');
            if (data.Success) {

                showSuccessToast(data.Title, data.Message);
                getListTab();

            } else {
                showWarningToast(data.Title, data.Message);
                $('#btnSave').removeAttr('disabled');
                $('#btnCancel').removeAttr('disabled');
            }
        },
        error: function (data) {
            console.log('Errores');
            console.log(data);

            showDangerToast(data.Title, data.Message);
        }
    });
}

function ConfirmDeleteSP(id) {
    var MyModal = $('#smModal');
    var modalContent = $('#smModalContent');

    modalContent.html('');

    MyModal.modal('show');

    $.ajax({
        url: '/SalesPlan/ConfirmDelete/' + id,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
        }
    });
}

function DeleteSP(id) {
    $.ajax({
        url: '/SalesPlan/Delete/' + id,
        type: 'POST',
        dataType: 'json',
        beforeSend: function () {
            $('#btnDelete').attr('disabled', true);
            $('#btnCancel').attr('disabled', true);
        },
        success: function (data) {
            console.log('Success');
            if (data.Success) {

                showSuccessToast(data.Title, data.Message);
                getListTab();
                $('#smModal').modal('hide');
                $('#smModalContent').html('');

            } else {

                showWarningToast(data.Title, data.Message);
                $('#btnDelete').removeAttr('disabled');
                $('#btnCancel').removeAttr('disabled');
            }
        },
        error: function (data) {
            console.log('Errores');
            console.log(data);

            showDangerToast(data.Title, data.Message);
        }
    });
}

function EditSP(id) {


    $.ajax({
        url: '/SalesPlan/Form/' + id,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
        },
        success: function (data) {
            $('#formSP').html(data);
        }
    });
}


function getComplements(id) {
    var MyModal = $('#mdModal');
    var modalContent = $('#mdModalContent');

    modalContent.html('');

    MyModal.modal('show');
    $.ajax({
        url: '/Complements/list?SalesPlanId='+id,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
            $('#SalesPlanID').val(id);
        }
    });
}