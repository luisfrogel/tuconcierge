﻿$(document).ready(function () {

    $('.nav-item').removeClass('active');
    $('.nav-link').removeClass('active');
    $('#navCoverages').addClass('active');

    getList();
});

function getList() {

    var url = '/Coverages/List';
    var table = $('#MyTable');

    var officeId = Number($('#selectOfficeFilter').val());
    var name = $('#NameFilter').val();
    var filter = false;
    var params = '';

    if (officeId !== 0) {
        params += 'OfficeId=' + officeId + '&';
        filter = true;
    }

    if (name !== '') {
        params += 'Name=' + name + '&';
        filter = true;
    }

    if (filter) {
        url += '?' + params.slice(0, -1);
    }

    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            table.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            table.html(data);
        }
    });
}

function Create() {

    var MyModal = $('#lgModal');
    var modalContent = $('#lgModalContent');

    modalContent.html('');
    MyModal.modal('show');

    $.ajax({
        url: '/Coverages/Form',
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
        }
    });
}

function Edit(id) {
    var MyModal = $('#lgModal');
    var modalContent = $('#lgModalContent');

    modalContent.html('');

    MyModal.modal('show');

    $.ajax({
        url: '/Coverages/Form/' + id,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
            $('#selectOffice').val($('#OfficeId').val());
        }
    });
}

function Save() {
    var name = $('#Name').val();
    var selectOffice = $('#selectOffice').val();
    var Lat = $('#Lat').val();
    var Long = $('#Long').val();
    var Ratio = $('#Ratio').val();

    var ExistErrors = false;

    if (name === '') {
        showDangerToast('Error de validación', 'El campo Nombre no puede estar vacío.');
        ExistErrors = true;
    }

    if (selectOffice == 0) {
        showDangerToast('Error de validación', 'Debe seleccionar un dato del campo Sucursal.');
        ExistErrors = true;
    }

    if (ExistErrors) {
        return;
    }

    var data = {
        Id: Number($('#Id').val()),
        Name: name,
        OfficeId: selectOffice,
        Lat: Lat,
        Long: Long,
        Ratio: Ratio
    };

    $.ajax({
        url: '/Coverages/Save',
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function () {
            $('#btnSave').attr('disabled', true);
            $('#btnCancel').attr('disabled', true);
        },
        success: function (data) {
            console.log('Success');
            if (data.Success) {

                showSuccessToast(data.Title, data.Message);
                getList();
                $('#lgModal').modal('hide');
                $('#lgModalContent').html('');

            } else {

                showWarningToast(data.Title, data.Message);
                $('#btnSave').removeAttr('disabled');
                $('#btnCancel').removeAttr('disabled');
            }
        },
        error: function (data) {
            console.log('Errores');
            console.log(data);

            showDangerToast(data.Title, data.Message);
        }
    });
}

function ConfirmDelete(id) {
    var MyModal = $('#smModal');
    var modalContent = $('#smModalContent');

    modalContent.html('');

    MyModal.modal('show');

    $.ajax({
        url: '/Coverages/ConfirmDelete/' + id,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
        }
    });
}

function Delete(id) {
    $.ajax({
        url: '/Coverages/Delete/' + id,
        type: 'POST',
        dataType: 'json',
        beforeSend: function () {
            $('#btnDelete').attr('disabled', true);
            $('#btnCancel').attr('disabled', true);
        },
        success: function (data) {
            console.log('Success');
            if (data.Success) {

                showSuccessToast(data.Title, data.Message);
                getList();
                $('#smModal').modal('hide');
                $('#smModalContent').html('');

            } else {

                showWarningToast(data.Title, data.Message);
                $('#btnDelete').removeAttr('disabled');
                $('#btnCancel').removeAttr('disabled');
            }
        },
        error: function (data) {
            console.log('Errores');
            console.log(data);

            showDangerToast(data.Title, data.Message);
        }
    });
}

function Details(id) {
    var MyModal = $('#mdModal');
    var modalContent = $('#mdModalContent');

    modalContent.html('');

    MyModal.modal('show');

    $.ajax({
        url: '/Coverages/Details/' + id,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
        }
    });
}