﻿function Logout() {

    $.ajax({
        url: '/Home/Logout',
        type: 'POST',
        dataType: 'html',
        success: function (data) {

            window.location.href = '/Home/Login';
        }
    });

}