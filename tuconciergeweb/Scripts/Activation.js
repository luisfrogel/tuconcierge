﻿$(document).ready(function () {

    setTimeout(activate, 2000);
});

function activate() {
  
    var data = {
        "Email": $('#email').val(),
        "Code": $('#code').val()
    };

    console.log(data);
   
    $.ajax({
        url: '/Account/Activate',
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function () {

        },
        success: function (data) {

            if (data.Success) {

                $('#procesando').attr('hidden', true);
                $('#activado').removeAttr('hidden');
                $('#login').removeAttr('hidden');

            } else {

                $('#procesando').attr('hidden', true);
                $('#errorMessage').html(data.Message);
                $('#error').removeAttr('hidden');
                $('#home').removeAttr('hidden');

            }
        },
        error: function (data) {
            console.log('Errores');
            console.log(data);

            showDangerToast(data.Title, data.Message);
        }
    });
}
