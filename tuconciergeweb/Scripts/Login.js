﻿function Save() {
    var email = $('#inputEmail').val();
    var password = $('#inputPassword').val();

    var ExistErrors = false;

    if (email === '') {
        showDangerToast('Error de validación', 'El campo Nombre no puede estar vacío.');
        ExistErrors = true;
    }

    if (password == 0) {
        showDangerToast('Error de validación', 'El campo Contraseña no puede estar vacío.');
        ExistErrors = true;
    }

    if (ExistErrors) {
        return;
    }

    var data = {
        Username: email,
        Password: password
    };

    $.ajax({
        url: '/Home/Login',
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function () {
            $('#btnSave').attr('disabled', true);
        },
        success: function (data) {
            console.log('Success');
            if (data.Success) {

                showSuccessToast(data.Title, data.Message);

                window.location.href = '/Home/Index';

            } else {

                showWarningToast(data.Title, data.Message);
                $('#btnSave').removeAttr('disabled');
            }
        },
        error: function (data) {
            console.log('Errores');
            console.log(data);

            showDangerToast(data.Title, data.Message);
        }
    });
}