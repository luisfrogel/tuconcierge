﻿$(document).ready(function () {

    $('.nav-item').removeClass('active');
    $('.nav-link').removeClass('active');
    $('#navServices').addClass('active');

    getList();
});

function getList() {

    var url = '/Services/List';
    var table = $('#MyTable');

    var typeServiceId = Number($('#selectTypeServiceFilter').val());
    var name = $('#NameFilter').val();
    var filter = false;
    var params = '';

    if (typeServiceId !== 0) {
        params += 'TypeServiceId=' + typeServiceId + '&';
        filter = true;
    }

    if (name !== '') {
        params += 'Name=' + name + '&';
        filter = true;
    }

    if (filter) {
        url += '?' + params.slice(0, -1);
    }

    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            table.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            table.html(data);
        }
    });
}

function Create() {

    var MyModal = $('#mdModal');
    var modalContent = $('#mdModalContent');

    modalContent.html('');
    MyModal.modal('show');

    $.ajax({
        url: '/Services/Form',
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
        }
    });
}

function Edit(id) {
    var MyModal = $('#mdModal');
    var modalContent = $('#mdModalContent');

    modalContent.html('');

    MyModal.modal('show');

    $.ajax({
        url: '/Services/Form/' + id,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
            $('#selectTypeService').val($('#TypeServiceId').val());
            $('#selectOffice').val($('#IdOffice').val());
        }
    });
}

function Save() {
    var name = $('#Name').val();
    var selectTypeService = Number($('#selectTypeService').val());
    var description = $('#Description').val();
    var selectOffice = $('#selectOffice').val();
    var recordatorios = $('#Recordatorios').val();

    var ExistErrors = false;

    if (name === '') {
        showDangerToast('Error de validación', 'El campo Nombre no puede estar vacío.');
        ExistErrors = true;
    }

    if (selectTypeService == 0) {
        showDangerToast('Error de validación', 'Debe seleccionar un dato del campo Tipo de servicio.');
        ExistErrors = true;
    }

    if (selectOffice == 0) {
        showWarningToast('¡Cuidado!', 'Debe de seleccionar un dato del campo Sucursal');
    }

    if (ExistErrors) {
        return;
    }

    var data = {
        Id: Number($('#Id').val()),
        Name: name,
        TypeServiceId: selectTypeService,
        Description: description,
        IdOffice: selectOffice,
        Recordatorios: recordatorios
    };

    $.ajax({
        url: '/Services/Save',
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function () {
            $('#btnSave').attr('disabled', true);
            $('#btnCancel').attr('disabled', true);
        },
        success: function (data) {
            console.log('Success');
            if (data.Success) {

                showSuccessToast(data.Title, data.Message);
                getList();
                $('#mdModal').modal('hide');
                $('#mdModalContent').html('');

            } else {

                showWarningToast(data.Title, data.Message);
                $('#btnSave').removeAttr('disabled');
                $('#btnCancel').removeAttr('disabled');
            }
        },
        error: function (data) {
            console.log('Errores');
            console.log(data);

            showDangerToast(data.Title, data.Message);
        }
    });
}

function ConfirmDelete(id) {
    var MyModal = $('#smModal');
    var modalContent = $('#smModalContent');

    modalContent.html('');

    MyModal.modal('show');

    $.ajax({
        url: '/Services/ConfirmDelete/' + id,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
        }
    });
}

function Delete(id) {
    $.ajax({
        url: '/Services/Delete/' + id,
        type: 'POST',
        dataType: 'json',
        beforeSend: function () {
            $('#btnDelete').attr('disabled', true);
            $('#btnCancel').attr('disabled', true);
        },
        success: function (data) {
            console.log('Success');
            if (data.Success) {

                showSuccessToast(data.Title, data.Message);
                getList();
                $('#smModal').modal('hide');
                $('#smModalContent').html('');

            } else {

                showWarningToast(data.Title, data.Message);
                $('#btnDelete').removeAttr('disabled');
                $('#btnCancel').removeAttr('disabled');
            }
        },
        error: function (data) {
            console.log('Errores');
            console.log(data);

            showDangerToast(data.Title, data.Message);
        }
    });
}

function Details(id) {
    var MyModal = $('#mdModal');
    var modalContent = $('#mdModalContent');

    modalContent.html('');

    MyModal.modal('show');

    $.ajax({
        url: '/Services/Details/' + id,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
        }
    });
}

function getPlans(id) {
    var MyModal = $('#lgModal');
    var modalContent = $('#lgModalContent');

    modalContent.html('');

    MyModal.modal('show');
    $.ajax({
        url: '/SalesPlan/ModalTab',
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            var loader = $('#Loader').clone();
            modalContent.html(loader.removeAttr('hidden'));
        },
        success: function (data) {
            modalContent.html(data);
            $('#ServiceID').val(id);
        }
    });
}