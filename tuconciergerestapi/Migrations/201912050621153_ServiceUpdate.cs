namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ServiceUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Services", "Observaciones", c => c.String(unicode: false, storeType: "text"));
            AddColumn("dbo.Services", "Recordatorios", c => c.String(unicode: false, storeType: "text"));
            AddColumn("dbo.Services", "IdOffice", c => c.Long(nullable: false));
            DropColumn("dbo.Services", "PricePerHour");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Services", "PricePerHour", c => c.Double(nullable: false));
            DropColumn("dbo.Services", "IdOffice");
            DropColumn("dbo.Services", "Recordatorios");
            DropColumn("dbo.Services", "Observaciones");
        }
    }
}
