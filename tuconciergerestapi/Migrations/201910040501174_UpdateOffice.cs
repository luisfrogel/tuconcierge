namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateOffice : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Offices", "StateId", "dbo.Companies");
            DropForeignKey("dbo.Offices", "MatrixId", "dbo.Offices");
            DropForeignKey("dbo.Offices", "StateId", "dbo.States");
            DropIndex("dbo.Offices", new[] { "StateId" });
            DropIndex("dbo.Offices", new[] { "MatrixId" });
            DropColumn("dbo.Offices", "StateId");
            DropColumn("dbo.Offices", "CompanyId");
            DropColumn("dbo.Offices", "MatrixId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Offices", "MatrixId", c => c.Long());
            AddColumn("dbo.Offices", "CompanyId", c => c.Long(nullable: false));
            AddColumn("dbo.Offices", "StateId", c => c.Long(nullable: false));
            CreateIndex("dbo.Offices", "MatrixId");
            CreateIndex("dbo.Offices", "StateId");
            AddForeignKey("dbo.Offices", "StateId", "dbo.States", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Offices", "MatrixId", "dbo.Offices", "Id");
            AddForeignKey("dbo.Offices", "StateId", "dbo.Companies", "Id", cascadeDelete: true);
        }
    }
}
