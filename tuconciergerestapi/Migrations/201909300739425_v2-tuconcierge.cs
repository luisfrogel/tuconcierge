namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v2tuconcierge : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.States", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.States", "TimeStamp", c => c.String());
            AddColumn("dbo.States", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.States", "RowVersion");
            DropColumn("dbo.States", "TimeStamp");
            DropColumn("dbo.States", "Status");
        }
    }
}
