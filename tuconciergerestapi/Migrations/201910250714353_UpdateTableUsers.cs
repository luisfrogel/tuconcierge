namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateTableUsers : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AspNetUsers", "LastName", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "LastName", c => c.String(nullable: false));
        }
    }
}
