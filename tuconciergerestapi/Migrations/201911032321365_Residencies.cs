namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Residencies : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Residences",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Location = c.String(),
                        Longitude = c.String(),
                        Latitude = c.String(),
                        Number = c.String(),
                        Street = c.String(),
                        References = c.String(),
                        TypeResidenceId = c.Long(nullable: false),
                        Status = c.Int(nullable: false),
                        TimeStamp = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TypeResidences", t => t.TypeResidenceId, cascadeDelete: true)
                .Index(t => t.TypeResidenceId);
            
            CreateTable(
                "dbo.TypeResidences",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(unicode: false, storeType: "text"),
                        Status = c.Int(nullable: false),
                        TimeStamp = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Residences", "TypeResidenceId", "dbo.TypeResidences");
            DropIndex("dbo.Residences", new[] { "TypeResidenceId" });
            DropTable("dbo.TypeResidences");
            DropTable("dbo.Residences");
        }
    }
}
