namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserCompany : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserCompanies",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.String(),
                        CompanyId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserCompanies");
        }
    }
}
