namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class urlphoto : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TypeServices", "Image", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TypeServices", "Image");
        }
    }
}
