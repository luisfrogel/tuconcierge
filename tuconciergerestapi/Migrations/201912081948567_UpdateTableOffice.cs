namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateTableOffice : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Coverages", "OfficeId", "dbo.Offices");
            DropIndex("dbo.Coverages", new[] { "OfficeId" });
            AddColumn("dbo.Offices", "Lat", c => c.String(nullable: false));
            AddColumn("dbo.Offices", "Long", c => c.String(nullable: false));
            AddColumn("dbo.Offices", "CountryId", c => c.Long(nullable: false));
            AlterColumn("dbo.Offices", "Address", c => c.String());
            AlterColumn("dbo.Offices", "PostalCode", c => c.String());
            DropColumn("dbo.Offices", "MatrixId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Offices", "MatrixId", c => c.Long());
            AlterColumn("dbo.Offices", "PostalCode", c => c.String(nullable: false));
            AlterColumn("dbo.Offices", "Address", c => c.String(nullable: false));
            DropColumn("dbo.Offices", "CountryId");
            DropColumn("dbo.Offices", "Long");
            DropColumn("dbo.Offices", "Lat");
            CreateIndex("dbo.Coverages", "OfficeId");
            AddForeignKey("dbo.Coverages", "OfficeId", "dbo.Offices", "Id", cascadeDelete: true);
        }
    }
}
