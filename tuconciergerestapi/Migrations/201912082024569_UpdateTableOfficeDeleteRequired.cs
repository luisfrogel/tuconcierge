namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateTableOfficeDeleteRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Offices", "Lat", c => c.String());
            AlterColumn("dbo.Offices", "Long", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Offices", "Long", c => c.String(nullable: false));
            AlterColumn("dbo.Offices", "Lat", c => c.String(nullable: false));
        }
    }
}
