namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SalesPlans : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SalesPlans",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(unicode: false, storeType: "text"),
                        Cost = c.Double(nullable: false),
                        ServicesId = c.Long(nullable: false),
                        Status = c.Int(nullable: false),
                        TimeStamp = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SalesPlans");
        }
    }
}
