namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Contact : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contracts", "TypeServiceId", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Contracts", "TypeServiceId");
        }
    }
}
