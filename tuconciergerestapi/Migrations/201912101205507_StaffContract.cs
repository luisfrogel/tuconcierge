namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StaffContract : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StaffContracts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ContractId = c.Long(nullable: false),
                        StaffId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.StaffContracts");
        }
    }
}
