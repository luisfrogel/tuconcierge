namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdminCompaniesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdminCompanies",
                c => new
                    {
                        CompanyId = c.Long(nullable: false),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.CompanyId, t.UserId });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AdminCompanies");
        }
    }
}
