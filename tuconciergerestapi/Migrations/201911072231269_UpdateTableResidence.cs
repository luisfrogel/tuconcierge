namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateTableResidence : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Residences", "InternalNumber", c => c.String());
            AddColumn("dbo.Residences", "ExternalNumber", c => c.String());
            AddColumn("dbo.Residences", "Crossings", c => c.String());
            DropColumn("dbo.Residences", "Number");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Residences", "Number", c => c.String());
            DropColumn("dbo.Residences", "Crossings");
            DropColumn("dbo.Residences", "ExternalNumber");
            DropColumn("dbo.Residences", "InternalNumber");
        }
    }
}
