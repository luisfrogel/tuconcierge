namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1tuconcierge : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        FiscalName = c.String(nullable: false),
                        ComercialName = c.String(nullable: false),
                        RFC = c.String(nullable: false),
                        FiscalRegime = c.String(),
                        Status = c.Int(nullable: false),
                        TimeStamp = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                        Status = c.Int(nullable: false),
                        TimeStamp = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Coverages",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Ratio = c.String(nullable: false),
                        Lat = c.String(nullable: false),
                        Long = c.String(nullable: false),
                        OfficeId = c.Long(nullable: false),
                        Status = c.Int(nullable: false),
                        TimeStamp = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Offices", t => t.OfficeId, cascadeDelete: true)
                .Index(t => t.OfficeId);
            
            CreateTable(
                "dbo.Offices",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Address = c.String(nullable: false),
                        PostalCode = c.String(nullable: false),
                        Picture = c.String(),
                        City = c.String(),
                        StateId = c.Long(nullable: false),
                        CompanyId = c.Long(nullable: false),
                        MatrixId = c.Long(),
                        Status = c.Int(nullable: false),
                        TimeStamp = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.StateId, cascadeDelete: true)
                .ForeignKey("dbo.Offices", t => t.MatrixId)
                .ForeignKey("dbo.States", t => t.StateId, cascadeDelete: true)
                .Index(t => t.StateId)
                .Index(t => t.MatrixId);
            
            CreateTable(
                "dbo.States",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        CountryId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.CountryId, cascadeDelete: true)
                .Index(t => t.CountryId);
            
            CreateTable(
                "dbo.CoverageServices",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CoverageId = c.Long(nullable: false),
                        ServiceId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Coverages", t => t.CoverageId, cascadeDelete: true)
                .ForeignKey("dbo.Services", t => t.ServiceId, cascadeDelete: true)
                .Index(t => t.CoverageId)
                .Index(t => t.ServiceId);
            
            CreateTable(
                "dbo.Services",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(unicode: false, storeType: "text"),
                        PricePerHour = c.Double(nullable: false),
                        TypeServiceId = c.Long(nullable: false),
                        Status = c.Int(nullable: false),
                        TimeStamp = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TypeServices", t => t.TypeServiceId, cascadeDelete: true)
                .Index(t => t.TypeServiceId);
            
            CreateTable(
                "dbo.TypeServices",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(unicode: false, storeType: "text"),
                        Status = c.Int(nullable: false),
                        TimeStamp = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Staffs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Email = c.String(),
                        PhoneNumber = c.String(),
                        Address = c.String(),
                        City = c.String(),
                        StateId = c.Long(nullable: false),
                        Name = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Birth = c.String(),
                        Gender = c.Int(nullable: false),
                        MaritalState = c.Int(nullable: false),
                        CURP = c.String(),
                        RFC = c.String(),
                        Status = c.Int(nullable: false),
                        TimeStamp = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.States", t => t.StateId, cascadeDelete: true)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.StaffServices",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        StaffId = c.Long(nullable: false),
                        ServiceId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Services", t => t.ServiceId, cascadeDelete: true)
                .ForeignKey("dbo.Staffs", t => t.StaffId, cascadeDelete: true)
                .Index(t => t.StaffId)
                .Index(t => t.ServiceId);
            
            AddColumn("dbo.AspNetUsers", "Name", c => c.String(nullable: false));
            AddColumn("dbo.AspNetUsers", "LastName", c => c.String(nullable: false));
            AddColumn("dbo.AspNetUsers", "Birth", c => c.String());
            AddColumn("dbo.AspNetUsers", "Gender", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "MaritalState", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "CURP", c => c.String());
            AddColumn("dbo.AspNetUsers", "RFC", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StaffServices", "StaffId", "dbo.Staffs");
            DropForeignKey("dbo.StaffServices", "ServiceId", "dbo.Services");
            DropForeignKey("dbo.Staffs", "StateId", "dbo.States");
            DropForeignKey("dbo.CoverageServices", "ServiceId", "dbo.Services");
            DropForeignKey("dbo.Services", "TypeServiceId", "dbo.TypeServices");
            DropForeignKey("dbo.CoverageServices", "CoverageId", "dbo.Coverages");
            DropForeignKey("dbo.Coverages", "OfficeId", "dbo.Offices");
            DropForeignKey("dbo.Offices", "StateId", "dbo.States");
            DropForeignKey("dbo.States", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.Offices", "MatrixId", "dbo.Offices");
            DropForeignKey("dbo.Offices", "StateId", "dbo.Companies");
            DropIndex("dbo.StaffServices", new[] { "ServiceId" });
            DropIndex("dbo.StaffServices", new[] { "StaffId" });
            DropIndex("dbo.Staffs", new[] { "StateId" });
            DropIndex("dbo.Services", new[] { "TypeServiceId" });
            DropIndex("dbo.CoverageServices", new[] { "ServiceId" });
            DropIndex("dbo.CoverageServices", new[] { "CoverageId" });
            DropIndex("dbo.States", new[] { "CountryId" });
            DropIndex("dbo.Offices", new[] { "MatrixId" });
            DropIndex("dbo.Offices", new[] { "StateId" });
            DropIndex("dbo.Coverages", new[] { "OfficeId" });
            DropColumn("dbo.AspNetUsers", "RFC");
            DropColumn("dbo.AspNetUsers", "CURP");
            DropColumn("dbo.AspNetUsers", "MaritalState");
            DropColumn("dbo.AspNetUsers", "Gender");
            DropColumn("dbo.AspNetUsers", "Birth");
            DropColumn("dbo.AspNetUsers", "LastName");
            DropColumn("dbo.AspNetUsers", "Name");
            DropTable("dbo.StaffServices");
            DropTable("dbo.Staffs");
            DropTable("dbo.TypeServices");
            DropTable("dbo.Services");
            DropTable("dbo.CoverageServices");
            DropTable("dbo.States");
            DropTable("dbo.Offices");
            DropTable("dbo.Coverages");
            DropTable("dbo.Countries");
            DropTable("dbo.Companies");
        }
    }
}
