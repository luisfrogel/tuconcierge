namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddKeyOffices : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Offices", "StateId", c => c.Long(nullable: false));
            AddColumn("dbo.Offices", "CompanyId", c => c.Long(nullable: false));
            AddColumn("dbo.Offices", "MatrixId", c => c.Long());
            CreateIndex("dbo.Offices", "StateId");
            CreateIndex("dbo.Offices", "CompanyId");
            CreateIndex("dbo.Offices", "MatrixId");
            AddForeignKey("dbo.Offices", "CompanyId", "dbo.Companies", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Offices", "MatrixId", "dbo.Offices", "Id");
            AddForeignKey("dbo.Offices", "StateId", "dbo.States", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Offices", "StateId", "dbo.States");
            DropForeignKey("dbo.Offices", "MatrixId", "dbo.Offices");
            DropForeignKey("dbo.Offices", "CompanyId", "dbo.Companies");
            DropIndex("dbo.Offices", new[] { "MatrixId" });
            DropIndex("dbo.Offices", new[] { "CompanyId" });
            DropIndex("dbo.Offices", new[] { "StateId" });
            DropColumn("dbo.Offices", "MatrixId");
            DropColumn("dbo.Offices", "CompanyId");
            DropColumn("dbo.Offices", "StateId");
        }
    }
}
