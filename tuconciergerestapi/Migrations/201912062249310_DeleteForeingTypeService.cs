namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteForeingTypeService : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Services", "TypeServiceId", "dbo.TypeServices");
            DropIndex("dbo.Services", new[] { "TypeServiceId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Services", "TypeServiceId");
            AddForeignKey("dbo.Services", "TypeServiceId", "dbo.TypeServices", "Id", cascadeDelete: true);
        }
    }
}
