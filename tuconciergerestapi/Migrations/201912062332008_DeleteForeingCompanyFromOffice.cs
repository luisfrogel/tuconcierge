namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteForeingCompanyFromOffice : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Offices", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Offices", "MatrixId", "dbo.Offices");
            DropForeignKey("dbo.Offices", "StateId", "dbo.States");
            DropIndex("dbo.Offices", new[] { "StateId" });
            DropIndex("dbo.Offices", new[] { "CompanyId" });
            DropIndex("dbo.Offices", new[] { "MatrixId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Offices", "MatrixId");
            CreateIndex("dbo.Offices", "CompanyId");
            CreateIndex("dbo.Offices", "StateId");
            AddForeignKey("dbo.Offices", "StateId", "dbo.States", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Offices", "MatrixId", "dbo.Offices", "Id");
            AddForeignKey("dbo.Offices", "CompanyId", "dbo.Companies", "Id", cascadeDelete: true);
        }
    }
}
