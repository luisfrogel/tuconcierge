namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Residenciesv2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Residences", "UserId", c => c.String(nullable: false));
            AlterColumn("dbo.Residences", "Location", c => c.String(nullable: false));
            AlterColumn("dbo.Residences", "Longitude", c => c.String(nullable: false));
            AlterColumn("dbo.Residences", "Latitude", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Residences", "Latitude", c => c.String());
            AlterColumn("dbo.Residences", "Longitude", c => c.String());
            AlterColumn("dbo.Residences", "Location", c => c.String());
            DropColumn("dbo.Residences", "UserId");
        }
    }
}
