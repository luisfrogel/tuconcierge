namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCodeEmailUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "CodeConfirmEmail", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "CodeConfirmEmail");
        }
    }
}
