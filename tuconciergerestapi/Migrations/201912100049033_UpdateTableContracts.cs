namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateTableContracts : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contracts", "Complements", c => c.String(nullable: false));
            AddColumn("dbo.Contracts", "DaysSelected", c => c.String());
            AddColumn("dbo.Contracts", "Calification", c => c.Long(nullable: false));
            AddColumn("dbo.Contracts", "Opinion", c => c.String(unicode: false, storeType: "text"));
            AlterColumn("dbo.Contracts", "PaymentStatus", c => c.Int(nullable: false));
            AlterColumn("dbo.Contracts", "AttentionStatus", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Contracts", "AttentionStatus", c => c.Long(nullable: false));
            AlterColumn("dbo.Contracts", "PaymentStatus", c => c.String());
            DropColumn("dbo.Contracts", "Opinion");
            DropColumn("dbo.Contracts", "Calification");
            DropColumn("dbo.Contracts", "DaysSelected");
            DropColumn("dbo.Contracts", "Complements");
        }
    }
}
