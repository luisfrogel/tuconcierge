namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Contract : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contracts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        OfficeId = c.Long(nullable: false),
                        SalesPlanId = c.Long(nullable: false),
                        ServiceId = c.Long(nullable: false),
                        Observations = c.String(),
                        StartDate = c.String(nullable: false),
                        StartTime = c.String(nullable: false),
                        Duration = c.Long(nullable: false),
                        Recurrence = c.String(),
                        TotalPrice = c.Double(nullable: false),
                        PaymentStatus = c.String(),
                        AttentionStatus = c.Long(nullable: false),
                        PaymentMethod = c.String(),
                        Status = c.Int(nullable: false),
                        TimeStamp = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Contracts");
        }
    }
}
