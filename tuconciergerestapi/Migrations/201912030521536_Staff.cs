namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Staff : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Staffs", "OfficeId", c => c.Long(nullable: false));
            AddColumn("dbo.Staffs", "FatherLastName", c => c.String(nullable: false));
            AddColumn("dbo.Staffs", "MotherLastName", c => c.String(nullable: false));
            DropColumn("dbo.Staffs", "LastName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Staffs", "LastName", c => c.String(nullable: false));
            DropColumn("dbo.Staffs", "MotherLastName");
            DropColumn("dbo.Staffs", "FatherLastName");
            DropColumn("dbo.Staffs", "OfficeId");
        }
    }
}
