namespace tuconciergerestapi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPhotoStaff : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Staffs", "StateId", "dbo.States");
            DropIndex("dbo.Staffs", new[] { "StateId" });
            AddColumn("dbo.Staffs", "Photo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Staffs", "Photo");
            CreateIndex("dbo.Staffs", "StateId");
            AddForeignKey("dbo.Staffs", "StateId", "dbo.States", "Id", cascadeDelete: true);
        }
    }
}
