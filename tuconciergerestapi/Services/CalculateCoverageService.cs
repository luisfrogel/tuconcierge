﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using tuconciergemodels;

namespace tuconciergerestapi.Services
{
    public class CalculateCoverageService
    {

        public bool IsInside(Coordinates checkPoint, CoverageOffice centerPoint)
        {
            var ky = 40000 / 360;

            var kx = Math.Cos(Math.PI * centerPoint.lat / 180.0) * ky;

            var dx = Math.Abs(centerPoint.lng - checkPoint.lng) * kx;

            var dy = Math.Abs(centerPoint.lat - checkPoint.lat) * ky;

            return Math.Sqrt(dx * dx + dy * dy) <= centerPoint.Ratio;
        }
    }
}