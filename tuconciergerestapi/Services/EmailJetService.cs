﻿using Mailjet.Client;
using Mailjet.Client.Resources;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;

namespace tuconciergerestapi.Services
{

    public class EmailJetService
    {
        public static void SendEmailSMTP(string To, string ToName, string Subject, string HtmlPart)
        {
            MailMessage msg = new MailMessage();

            msg.From = new MailAddress("contact@devfrr.com", "noreply");
            msg.To.Add(new MailAddress(To, ToName));

            msg.Subject = Subject;
            msg.IsBodyHtml = true;
            msg.Body = HtmlPart;

            SmtpClient client = new SmtpClient("in-v3.mailjet.com", 587);
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("4871bada29944ebe736a81ad050d2fa6", "96f7d03b1d56f906d616f0ae569bc869");

            client.Send(msg);
        }

        public static async Task<bool> SendEmail(string To, string ToName, string Subject, string HtmlPart)
        {
            MailjetClient client = new MailjetClient("4871bada29944ebe736a81ad050d2fa6", "96f7d03b1d56f906d616f0ae569bc869")
            {
                Version = ApiVersion.V3_1,
            };
            MailjetRequest request = new MailjetRequest
            {
                Resource = Send.Resource,
            }
               .Property(Send.Messages, new JArray {
                   new JObject {
                       {"From",
                            new JObject {
                              {"Email", "contact@devfrr.com"},
                              {"Name", "noreply"}
                            }
                       },
                       {"To",
                            new JArray {
                                new JObject {
                                    {"Email", To },
                                    {"Name", ToName}
                                }
                            }
                       },
                       {"Subject", Subject},
                       //{"TextPart", TextPart},
                       {"HTMLPart", HtmlPart}
                   }
               });
            MailjetResponse response = await client.PostAsync(request);
            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string GenerateEmailVerification(string Code)
        {
            var htmlBody = string.Format("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html xmlns='http://www.w3.org/1999/xhtml'><head> <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/> <meta name='viewport' content='width=device-width, initial-scale=1.0'/> <title>Correo</title> <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap' rel='stylesheet'> <style>body{{font-family: 'Source Sans Pro', Arial, sans-serif !important;}}a{{text-decoration: none !important;}}</style></head><body style='margin: 0; padding: 5% 5% 5% 5%; background: #f1f1f1'> <div style=' background: #ffffff; padding: 4% 0 4% 0; width: 95%; margin: 0 auto;'> <table style='margin: 0 auto; overflow-wrap: break-word; font-size: 14px;' cellpadding='0' cellspacing='0' width='80%'> <tr> <td> <img src='https://res.cloudinary.com/lfrr/image/upload/v1572799110/logo-h.png' width='300' alt='Logo'/> <br><br><br></td></tr><tr> <td> <span style='font-size:24px;'>¡Bienvenido a nuestra plataforma!</span><br><br></td></tr><tr> <td style='overflow-wrap: break-word;'> Gracias por registrarse. <br>Para continuar usando la aplicación, ingrese el siguiente código de verificación <b>{0}</b>. </td></tr><tr> <td><br><br><br></td></tr></table> </div><div style='padding: 5% 5% 5% 5%;'> <table style='margin: 0 auto; font-size: 14px;' cellpadding='0' cellspacing='0' width='95%'> <tr> <td style='text-align: center;'> La información contenida en este mensaje fue provista por <a href='https://tuconcierge.azurewebsites.net'>tuconcierge.azurewebsites.net</a><br>. </td></tr><tr> <td style='text-align: center;'> <br><br><br>Derechos Reservados © {1} <b><a href='https://tuconcierge.azurewebsites.net' style='text-decoration: none; color: #000000;'>tuconcierge.azurewebsites.net</a></b><br>info@tuconcierge.azurewebsites.net <br>+52 999-511-7982<br>Yucatán, México. </td></tr></table> </div></body></html>",
                Code, DateTime.Now.Year);

            return htmlBody;
        }

        public static string GenerateEmailAdminActivation(string username, string password, string link)
        {
            var htmlBody = string.Format("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html xmlns='http://www.w3.org/1999/xhtml'><head> <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/> <meta name='viewport' content='width=device-width, initial-scale=1.0'/> <title>Correo</title> <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap' rel='stylesheet'> <style>body{{font-family: 'Source Sans Pro', Arial, sans-serif !important;}}a{{text-decoration: none !important;}}</style></head><body style='margin: 0; padding: 5% 5% 5% 5%; background: #f1f1f1'> <div style=' background: #ffffff; padding: 4% 0 4% 0; width: 95%; margin: 0 auto;'> <table style='margin: 0 auto; overflow-wrap: break-word; font-size: 14px;' cellpadding='0' cellspacing='0' width='80%'> <tr> <td> <img src='https://res.cloudinary.com/lfrr/image/upload/v1572799110/logo-h.png' width='300' alt='Logo'/> <br><br><br></td></tr><tr> <td> <span style='font-size:24px;'>¡Bienvenido a nuestra plataforma!</span><br><br></td></tr><tr> <td style='overflow-wrap: break-word;'> Gracias por confiar en nosotros, estamos a su disposición ante cualquier duda o aclaración. <br>Para poder acceder a la plataforma tendrá que activar su cuenta dando clic en el botón Activar cuenta, posteriormente será redirigido a una página para finalizar su registro, por último podrá acceder a la plataforma iniciando sesión con las credenciales que en este correo electrónico se encuentran. <br>Recuerde que esta es una contraseña provicional, se recomienda actualizar su contraseña; podrá hacerlo accediendo a Mi perfil. <br><br>Credenciales de acceso:<br><br>Usuario: <b>{0}</b><br>Contraseña: <b>{1}</b><br></td></tr><tr> <td style='text-align: center;'> <br><br><a href='{2}''> <div style='background: #F33B2A; width: 100%; text-align: center; padding: 16px 0px 16px 0px;'> <span style='font-size:16px; color: #ffffff;'><b>Activar cuenta</b></span> </div></a> <br><br><br></td></tr></table> </div><div style='padding: 5% 5% 5% 5%;'> <table style='margin: 0 auto; font-size: 14px;' cellpadding='0' cellspacing='0' width='95%'> <tr> <td style='text-align: center;'> La información contenida en este mensaje fue provista por <a href='https://tuconcierge.azurewebsites.net'>tuconcierge.azurewebsites.net</a><br>. </td></tr><tr> <td style='text-align: center;'> <br><br><br>Derechos Reservados ©{3}<b><a href='https://tuconcierge.azurewebsites.net' style='text-decoration: none; color: #000000;'>tuconcierge.azurewebsites.net</a></b><br>info@tuconcierge.azurewebsites.net <br>+52 999-511-7982<br>Yucatán, México. </td></tr></table> </div></body></html>",
                username, password, link, DateTime.Now.Year);

            return htmlBody;
        }

        public static string GenerateCodeVerification()
        {
            string code = "";

            for (int i = 0; i <= 5; i++)
            {
                var guid = Guid.NewGuid();
                var justNumbers = new String(guid.ToString().Where(Char.IsDigit).ToArray());
                var seed = int.Parse(justNumbers.Substring(0, 4));

                var random = new Random(seed);
                var value = random.Next(0, 9);

                code += value.ToString();
            }

            return code;
        }
    }


}