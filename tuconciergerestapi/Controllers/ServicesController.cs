﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using tuconciergemodels;
using tuconciergerestapi.Models;
using tuconciergerestapi.Services;

namespace tuconciergerestapi.Controllers
{
    [Authorize]
    public class ServicesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;

        public ServicesController()
        {
        }

        public ServicesController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [Route("api/Services/App")]
        public HttpResponseMessage GetServicesAppAsync(Enums.ModelStatus Status = Enums.ModelStatus.Active, string Name = "", long TypeServiceId = 0, long ResidenceId = 0)
        {

            //// Se busca lat y lng de la residencia para verificar si tiene cobertura
            //var checkPoint = db.Residences.Where(x => x.Id == ResidenceId).Select(x => new Coordinates()
            //{
            //    lat = double.Parse(x.Latitude),
            //    lng = double.Parse(x.Longitude)
            //}).FirstOrDefault();

            //// Se obtienen todas las zonas de covertura registradas
            //var coverages = db.Coverages.Select(x => new CoverageOffice()
            //{
            //    CoverageId = x.Id,
            //    OfficeId = x.OfficeId,
            //    lat = double.Parse(x.Lat),
            //    lng = double.Parse(x.Long),
            //    Ratio = double.Parse(x.Ratio)
            //}).ToList();

            //var calculatorCoverage = new CalculateCoverageService();

            //// Lista de las oficinas que tienen covertura en la zona  que se encuentra de la residencia
            //var Offices = new List<long>(); 

            // // Se verifica la residencia para ver si esta dentro de alguna de las zonas de cobertura
            // // Si la residencia esta dentro de una cobertura se agrega el OfficeId a otra lista.
            //foreach(var item in coverages)
            //{
            //    if(calculatorCoverage.IsInside(checkPoint, item))
            //    {
            //        Offices.Add(item.OfficeId);
            //    }
            //}

            

            var data = db.Services.Where(x => x.Status == Status);

            if(TypeServiceId != 0)
            {
                data = data.Where(x => x.TypeServiceId == TypeServiceId);
            }

            if (!Name.Equals(""))
            {
                data = data.Where(x => x.Name == Name);
            }

            var list = data.Select(x => new ServiceViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                TimeStamp = x.TimeStamp,
                Observaciones = x.Observaciones,
                Recordatorios = x.Recordatorios,
                TypeServiceId = x.TypeServiceId,
                TypeService = db.TypeServices.Where(z => z.Id == x.TypeServiceId).FirstOrDefault(),
                IdOffice = x.IdOffice,
                Office = db.Offices.Where(z => z.Id == x.IdOffice).FirstOrDefault()
            }).AsEnumerable();

            return Request.CreateResponse(HttpStatusCode.OK, new { data = list });
        }

        [Route("api/Services/App")]
        public async Task<HttpResponseMessage> GetServicesAppAsync(long ServiceId)
        {
            Service service = await db.Services.FindAsync(ServiceId);

            if (service == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, new { Message = "NotFound" });
            }


            var salesPlansAppViewModel = db.SalesPlans.Where(x => x.ServicesId == service.Id).Select(x => new SalesPlansAppViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                Cost = x.Cost,
                Complements = db.Complements.Where(z => z.SalesPlanId == x.Id).Select(z => new ComplementAppViewModel()
                {
                    Id = z.Id,
                    Name = z.Name,
                    Description = z.Description,
                    Cost = z.Cost
                }).AsEnumerable()
            }).AsEnumerable();

            var serviceAppViewModel = new ServiceAppViewModel()
            {
                Id = service.Id,
                Name = service.Name,
                Description = service.Description,
                Observaciones = service.Observaciones,
                Recordatorios = service.Recordatorios,
                Office = await db.Offices.FindAsync(service.IdOffice),
                TypeService = await db.TypeServices.FindAsync(service.TypeServiceId),
                TimeStamp = service.TimeStamp,
                Plans = salesPlansAppViewModel
            };

            return Request.CreateResponse(HttpStatusCode.OK, new { data = serviceAppViewModel });
        }


        // GET: api/Services
        public async Task<IEnumerable<ServiceViewModel>> GetServicesAsync(Enums.ModelStatus Status = Enums.ModelStatus.Active, string Name = "", long TypeServiceId = 0, long IdOffice = 0)
        {
            var data = db.Services.Where(x => x.Status == Status);

            if (TypeServiceId != 0)
            {
                data = data.Where(x => x.TypeServiceId == TypeServiceId);
            }

            if (!Name.Equals(""))
            {
                data = data.Where(x => x.Name.Contains(Name));
            }

            if (IdOffice != 0)
            {
                data = data.Where(x => x.IdOffice == IdOffice);
            }
            else
            {
                var user = await UserManager.FindByEmailAsync(User.Identity.GetUserName());

                var usercompany = db.UserCompanies.Where(x => x.UserId == user.Id).FirstOrDefault();

                var offices = db.Offices.Where(x => x.CompanyId == usercompany.CompanyId).Select(x => x.Id).ToArray();

                data = data.Where(x => offices.Contains(x.IdOffice));
            }

            var list = data.Select(x => new ServiceViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                TimeStamp = x.TimeStamp,
                Observaciones = x.Observaciones,
                Recordatorios = x.Recordatorios,
                TypeServiceId = x.TypeServiceId,
                TypeService = db.TypeServices.Where(z => z.Id == x.TypeServiceId).FirstOrDefault(),
                IdOffice = x.IdOffice,
                Office = db.Offices.Where(z => z.Id == x.IdOffice).FirstOrDefault()
            }).AsEnumerable();

            return list;
        }

        // GET: api/Services/5
        [ResponseType(typeof(Service))]
        public async Task<IHttpActionResult> GetService(long id)
        {
            Service service = await db.Services.FindAsync(id);
            if (service == null)
            {
                return NotFound();
            }

            var viewModel = new ServiceViewModel()
            {
                Id = service.Id,
                Name = service.Name,
                Description = service.Description,
                Observaciones = service.Observaciones,
                Recordatorios = service.Recordatorios,
                TypeServiceId = service.TypeServiceId,
                TypeService = db.TypeServices.Where(z => z.Id == service.TypeServiceId).FirstOrDefault(),
                IdOffice = service.IdOffice,
                Office = db.Offices.Where(z => z.Id == service.IdOffice).FirstOrDefault(),
                TimeStamp = service.TimeStamp
            };

            return Ok(viewModel);
        }

        // PUT: api/Services/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutService(long id, Service model)
        {
            Service service = await db.Services.FindAsync(id);
            if(service == null)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            service.Name = model.Name;
            service.Description = model.Description;
            service.Observaciones = model.Observaciones;
            service.Recordatorios = model.Recordatorios;
            service.TypeServiceId = service.TypeServiceId;
            service.IdOffice = service.IdOffice;
            service.Status = service.Status;

            db.Entry(service).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ServiceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Services
        [ResponseType(typeof(Service))]
        public async Task<IHttpActionResult> PostService(Service service)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            service.TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt");

            db.Services.Add(service);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = service.Id }, service);
        }

        // DELETE: api/Services/5
        [ResponseType(typeof(Service))]
        public async Task<IHttpActionResult> DeleteService(long id)
        {
            Service service = await db.Services.FindAsync(id);
            if (service == null)
            {
                return NotFound();
            }

            service.Status = Enums.ModelStatus.Delete;

            db.Entry(service).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ServiceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.Accepted);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ServiceExists(long id)
        {
            return db.Services.Count(e => e.Id == id) > 0;
        }
    }
}