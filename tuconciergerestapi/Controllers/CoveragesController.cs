﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using tuconciergemodels;
using tuconciergerestapi.Models;

namespace tuconciergerestapi.Controllers
{
    [Authorize]
    public class CoveragesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;

        public CoveragesController()
        {
        }

        public CoveragesController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: api/Coverages
        public async Task<IEnumerable<CoverageViewModel>> GetCoveragesAsync(Enums.ModelStatus Status = Enums.ModelStatus.Active, string Name = "", long OfficeId = 0)
        {
            var data = db.Coverages.Where(x => x.Status == Status);

            if (!Name.Equals(""))
            {
                data = data.Where(x => x.Name.Contains(Name));
            }

            if (OfficeId != 0)
            {
                data = data.Where(x => x.OfficeId == OfficeId);
            }
            else
            {
                var user = await UserManager.FindByEmailAsync(User.Identity.GetUserName());

                var usercompany = db.UserCompanies.Where(x => x.UserId == user.Id).FirstOrDefault();

                var offices = db.Offices.Where(x => x.CompanyId == usercompany.CompanyId).Select(x => x.Id).ToArray();

                data = data.Where(x => offices.Contains(x.OfficeId));
            }

            var list = data.Select(x => new CoverageViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                Lat = x.Lat,
                Long = x.Long,
                Ratio = x.Ratio,
                OfficeId = x.OfficeId,
                TimeStamp = x.TimeStamp,
                Office = db.Offices.Where(z => z.Id == x.OfficeId).FirstOrDefault()
            }).AsEnumerable();

            return list;
        }

        // GET: api/Coverages/5
        [ResponseType(typeof(CoverageViewModel))]
        public async Task<IHttpActionResult> GetCoverage(long id)
        {
            Coverage x = await db.Coverages.FindAsync(id);
            if (x == null)
            {
                return NotFound();
            }

            var viewModel = new CoverageViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                Lat = x.Lat,
                Long = x.Long,
                Ratio = x.Ratio,
                OfficeId = x.OfficeId,
                TimeStamp = x.TimeStamp,
                Office = db.Offices.Where(z => z.Id == x.OfficeId).FirstOrDefault()
            };

            return Ok(viewModel);
        }

        // PUT: api/Coverages/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCoverage(long id, Coverage model)
        {
            Coverage coverage = await db.Coverages.FindAsync(id);
            if (coverage == null)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            coverage.Name = model.Name;
            coverage.Lat = model.Lat;
            coverage.Long = model.Long;
            coverage.Ratio = model.Ratio;
            coverage.OfficeId = model.OfficeId;

            db.Entry(coverage).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CoverageExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Coverages
        [ResponseType(typeof(Coverage))]
        public async Task<IHttpActionResult> PostCoverage(Coverage coverage)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            coverage.TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt");

            db.Coverages.Add(coverage);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = coverage.Id }, coverage);
        }

        // DELETE: api/Coverages/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> DeleteCoverage(long id)
        {
            Coverage coverage = await db.Coverages.FindAsync(id);
            if (coverage == null)
            {
                return NotFound();
            }

            coverage.Status = Enums.ModelStatus.Delete;

            db.Entry(coverage).State = EntityState.Modified;


            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CoverageExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.Accepted);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CoverageExists(long id)
        {
            return db.Coverages.Count(e => e.Id == id) > 0;
        }
    }
}