﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using tuconciergemodels;
using tuconciergerestapi.Models;

namespace tuconciergerestapi.Controllers
{
    [Authorize]
    public class OfficesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private ApplicationUserManager _userManager;

        public OfficesController()
        {
        }

        public OfficesController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: api/Offices
        public async Task<IEnumerable<OfficeViewModel>> GetOfficesAsync(Enums.ModelStatus Status = Enums.ModelStatus.Active, string Name = "")
        {
            var user = await UserManager.FindByEmailAsync(User.Identity.GetUserName());

            var usercompany = db.UserCompanies.Where(x => x.UserId == user.Id).FirstOrDefault();

            var data = db.Offices.Where(x => x.Status == Status && x.CompanyId == usercompany.CompanyId);

            if (!Name.Equals(""))
            {
                data = data.Where(x => x.Name.Contains(Name));
            }

            var list = data.Select(x => new OfficeViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                Address = x.Address,
                PostalCode = x.PostalCode,
                Picture = x.Picture,
                City = x.City,
                StateId = x.StateId,
                CompanyId = x.CompanyId,
                CountryId = x.CountryId,
                Lat = x.Lat,
                Long = x.Long
            }).AsEnumerable();

            return list;
        }

        // GET: api/Offices/5
        [ResponseType(typeof(OfficeViewModel))]
        public async Task<IHttpActionResult> GetOffice(long id)
        {
            Office office = await db.Offices.FindAsync(id);
            if (office == null)
            {
                return NotFound();
            }

            var viewModel = new OfficeViewModel()
            {
                Id = office.Id,
                Name = office.Name,
                Address = office.Address,
                PostalCode = office.PostalCode,
                Picture = office.Picture,
                City = office.City,
                StateId = office.StateId,
                CompanyId = office.CompanyId,
                CountryId = office.CountryId,
                Lat = office.Lat,
                Long = office.Long
            };

            return Ok(viewModel);
        }

        // PUT: api/Offices/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutOffice(long id, Office model)
        {
            var user = await UserManager.FindByEmailAsync(User.Identity.GetUserName());

            var usercompany = db.UserCompanies.Where(x => x.UserId == user.Id).FirstOrDefault();

            Office office = await db.Offices.FindAsync(id);
            if (office == null)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            office.Name = model.Name;
            office.Address = model.Address;
            office.PostalCode = model.PostalCode;
            office.Picture = model.Picture;
            office.Status = model.Status;
            office.StateId = model.StateId;
            office.CompanyId = model.CompanyId;
            office.CountryId = model.CountryId;
            office.CompanyId = usercompany.CompanyId;
            office.Lat = model.Lat;
            office.Long = model.Long;

            db.Entry(office).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OfficeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Offices
        [ResponseType(typeof(Office))]
        public async Task<IHttpActionResult> PostOffice(Office office)
        {
            var user = await UserManager.FindByEmailAsync(User.Identity.GetUserName());

            var usercompany = db.UserCompanies.Where(x => x.UserId == user.Id).FirstOrDefault();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            office.CompanyId = usercompany.CompanyId;
            office.TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt");

            try
            {
                db.Offices.Add(office);
                await db.SaveChangesAsync();
            }
            catch(Exception)
            {
                return StatusCode(HttpStatusCode.InternalServerError);
            }

            

            return CreatedAtRoute("DefaultApi", new { id = office.Id }, office);
        }

        // DELETE: api/Offices/5
        [ResponseType(typeof(Office))]
        public async Task<IHttpActionResult> DeleteOffice(long id)
        {
            Office office = await db.Offices.FindAsync(id);
            if (office == null)
            {
                return NotFound();
            }

            office.Status = Enums.ModelStatus.Delete;

            db.Entry(office).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OfficeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.Accepted);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OfficeExists(long id)
        {
            return db.Offices.Count(e => e.Id == id) > 0;
        }
    }
}