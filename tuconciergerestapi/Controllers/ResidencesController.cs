﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using tuconciergemodels;
using tuconciergerestapi.Models;

namespace tuconciergerestapi.Controllers
{
    [Authorize]
    public class ResidencesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private ApplicationUserManager _userManager;

        public ResidencesController() { }

        public ResidencesController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: api/Residences
        public async Task<HttpResponseMessage> GetResidencesAsync(Enums.ModelStatus Status = Enums.ModelStatus.Active)
        {
            var user = await UserManager.FindByEmailAsync(User.Identity.GetUserName());

            var data = db.Residences.Where(x => x.Status == Status && x.UserId == user.Id);

            var list = data.Select(x => new ResidenceViewModel()
            {
                Id = x.Id,
                Location = x.Location,
                Latitude = x.Latitude,
                Longitude = x.Longitude,
                ExternalNumber = x.ExternalNumber,
                InternalNumber = x.InternalNumber,
                Street = x.Street,
                References = x.References,
                Crossings = x.Crossings,
                TypeResidence = db.TypeResidences.Where(z => z.Id == x.TypeResidenceId).FirstOrDefault(),
                UserId = x.UserId,
                TimeStamp = x.TimeStamp
            }).AsEnumerable();

            return Request.CreateResponse(HttpStatusCode.OK, new { data = list });
        }

        // GET: api/Residences/5
        [ResponseType(typeof(Residences))]
        public async Task<HttpResponseMessage> GetResidences(long id)
        {
            Residences residences = await db.Residences.FindAsync(id);
            if (residences == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            var viewModel = new Residences()
            {
                Id = residences.Id,
                Location = residences.Location,
                Latitude = residences.Latitude,
                Longitude = residences.Longitude,
                ExternalNumber = residences.ExternalNumber,
                InternalNumber = residences.InternalNumber,
                Street = residences.Street,
                Crossings = residences.Crossings,
                References = residences.References,
                TypeResidence = db.TypeResidences.Where(z => z.Id == residences.TypeResidenceId).FirstOrDefault(),
                TypeResidenceId = residences.TypeResidenceId,
                Status = residences.Status,
                UserId = residences.UserId,
                TimeStamp = residences.TimeStamp
            };

            return Request.CreateResponse(HttpStatusCode.OK, new { data = viewModel });
        }

        // PUT: api/Residences/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutResidences(long id, Residences model)
        {
            Residences residences = await db.Residences.FindAsync(id);
            if (residences == null)
            {
                return NotFound();
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            residences.Location = model.Location;
            residences.Latitude = model.Latitude;
            residences.Longitude = model.Longitude;
            residences.ExternalNumber = model.ExternalNumber;
            residences.InternalNumber = model.InternalNumber;
            residences.Street = model.Street;
            residences.Crossings = model.Latitude;
            residences.References = model.Latitude;
            residences.Latitude = model.Latitude;
            residences.TypeResidenceId = model.TypeResidenceId;
            residences.Status = model.Status;

            db.Entry(residences).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ResidencesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Residences
        [ResponseType(typeof(Residences))]
        public async Task<IHttpActionResult> PostResidences(Residences residences)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            residences.TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt");
            residences.Status = Enums.ModelStatus.Active;


            db.Residences.Add(residences);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = residences.Id }, residences);
        }

        // DELETE: api/Residences/5
        [ResponseType(typeof(Residences))]
        public async Task<IHttpActionResult> DeleteResidences(long id)
        {
            Residences residences = await db.Residences.FindAsync(id);
            if (residences == null)
            {
                return NotFound();
            }

            residences.Status = Enums.ModelStatus.Delete;

            db.Entry(residences).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ResidencesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.Accepted);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ResidencesExists(long id)
        {
            return db.Residences.Count(e => e.Id == id) > 0;
        }
    }
}