﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using tuconciergemodels;
using tuconciergerestapi.Models;

namespace tuconciergerestapi.Controllers
{
    [Authorize]
    public class ComplementsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Complements
        public HttpResponseMessage GetComplements(long SalesPlanId, Enums.ModelStatus Status = Enums.ModelStatus.Active, string Name = "")
        {
            var data = db.Complements.Where(x => x.Status == Status);

            if (!Name.Equals(""))
            {
                data = data.Where(x => x.Name.Contains(Name));
            }
            if (SalesPlanId != 0)
            {
                data = data.Where(x => x.SalesPlanId == SalesPlanId);
            }
            var list = data.Select(x => new ComplementViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                Cost = x.Cost,
                TimeStamp = x.TimeStamp,
                SalesPlans = db.SalesPlans.Where(z => z.Id == x.SalesPlanId).FirstOrDefault()
            }).AsEnumerable();

            return Request.CreateResponse(HttpStatusCode.OK, list );
        }

        // GET: api/Complements/5
        [ResponseType(typeof(Complement))]
        public async Task<HttpResponseMessage> GetComplement(long id)
        {
            Complement complement = await db.Complements.FindAsync(id);

            if (complement == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, new { Message = "NotFound" });
            }

            var viewModel = new ComplementViewModel()
            {
                Id = complement.Id,
                Name = complement.Name,
                Description = complement.Description,
                Cost = complement.Cost,
                TimeStamp = complement.TimeStamp,
                SalesPlans = db.SalesPlans.Where(z => z.Id == complement.SalesPlanId).FirstOrDefault()

            };

            return Request.CreateResponse(HttpStatusCode.OK, viewModel );
        }

        // PUT: api/Complements/5
        [ResponseType(typeof(void))]
        public async Task<HttpResponseMessage> PutComplement(long id, Complement model)
        {
            Complement complement = await db.Complements.FindAsync(id);
            if (complement == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, new { Message = "NotFound" });

            }

            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { data = ModelState });
            }
            complement.Name = model.Name;
            complement.Description = model.Description;
            complement.Cost = model.Cost;
            complement.Status = model.Status;
            complement.SalesPlanId = model.SalesPlanId;

            db.Entry(complement).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ComplementExists(id))
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, new { Message = "NotFound" });
                }
                else
                {
                    throw;
                }
            }

            return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Done" });
        }

        // POST: api/Complements
        [ResponseType(typeof(Complement))]
        public IHttpActionResult PostComplement(Complement complement)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            complement.TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt");


            db.Complements.Add(complement);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = complement.Id }, complement);
        }

        // DELETE: api/Complements/5
        [ResponseType(typeof(Complement))]
        public async Task<HttpResponseMessage> DeleteComplement(long id)
        {
            Complement complement = await db.Complements.FindAsync(id);
            if (complement == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, new { Message = "NotFound" });
            }

            complement.Status = Enums.ModelStatus.Delete;

            db.Entry(complement).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ComplementExists(id))
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, new { Message = "NotFound" });
                }
                else
                {
                    throw;
                }
            }

            return Request.CreateResponse(HttpStatusCode.Accepted, new { Message = "NotFound" });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ComplementExists(long id)
        {
            return db.Complements.Count(e => e.Id == id) > 0;
        }
    }
}