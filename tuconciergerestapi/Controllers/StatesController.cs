﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using tuconciergemodels;
using tuconciergerestapi.Models;

namespace tuconciergerestapi.Controllers
{
    [Authorize]
    public class StatesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/States
        public IEnumerable<StateViewModel> GetStates(Enums.ModelStatus Status = Enums.ModelStatus.Active, long CountryId = 0, string Name = "")
        {
            var data = db.States.Where(x => x.Status == Status);

            if (CountryId != 0)
            {
                data = data.Where(x => x.CountryId == CountryId);
            }

            if (!Name.Equals(""))
            {
                data = data.Where(x => x.Name.Contains(Name));
            }

            var list = data.Select(x => new StateViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                CountryId = x.CountryId,
                TimeStamp = x.TimeStamp,
                Country = db.Countries.Where(z => z.Id == x.CountryId).FirstOrDefault()
            }).AsEnumerable();

            return list;
        }

        // GET: api/States/5
        [ResponseType(typeof(StateViewModel))]
        public async Task<IHttpActionResult> GetState(long id)
        {
            State state = await db.States.FindAsync(id);

            if (state == null)
            {
                return NotFound();
            }

            var viewModel = new StateViewModel()
            {
                Id = state.Id,
                Name = state.Name,
                CountryId = state.CountryId,
                TimeStamp = state.TimeStamp,
                Country = db.Countries.Where(z => z.Id == state.CountryId).FirstOrDefault()
            };

            return Ok(viewModel);
        }

        // PUT: api/States/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutState(long id, State model)
        {
            State state = await db.States.FindAsync(id);
            if (state == null)
            {
                return NotFound();
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            state.Name = model.Name;
            state.CountryId = model.CountryId;
            state.Status = model.Status;

            db.Entry(state).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StateExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/States
        [ResponseType(typeof(State))]
        public async Task<IHttpActionResult> PostState(State state)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            state.TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt");

            db.States.Add(state);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = state.Id }, state);
        }

        // DELETE: api/States/5
        [ResponseType(typeof(State))]
        public async Task<IHttpActionResult> DeleteState(long id)
        {
            State state = await db.States.FindAsync(id);
            if (state == null)
            {
                return NotFound();
            }

            state.Status = Enums.ModelStatus.Delete;

            db.Entry(state).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StateExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.Accepted);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool StateExists(long id)
        {
            return db.States.Count(e => e.Id == id) > 0;
        }
    }
}