﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using tuconciergemodels;
using tuconciergerestapi.Models;

namespace tuconciergerestapi.Controllers
{
    [Authorize]
    public class TypeServicesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/TypeServices/App
        [Route("api/TypeServices/App")]
        public HttpResponseMessage GetTypeServicesAsync(Enums.ModelStatus Status = Enums.ModelStatus.Active, long TypeServiceId = 0)
        {
            var list = db.TypeServices.Where(x => x.Status == Status);

            return Request.CreateResponse(HttpStatusCode.OK, new { data = list });

        }

        // GET: api/TypeServices
        public IQueryable<TypeService> GetTypeServicesAsync(Enums.ModelStatus Status = Enums.ModelStatus.Active, string Name = "")
        {
            var data = db.TypeServices.Where(x => x.Status == Status);

            if (!Name.Equals(""))
            {
                data = data.Where(x => x.Name.Contains(Name));
            }

            return data;

        }

        // GET: api/TypeServices/5
        [ResponseType(typeof(TypeService))]
        public async Task<IHttpActionResult> GetTypeService(long id)
        {
            TypeService typeService = await db.TypeServices.FindAsync(id);
            if (typeService == null)
            {
                return NotFound();
            }

            return Ok(typeService);
        }

        // PUT: api/TypeServices/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTypeService(long id, TypeService model)
        {
            TypeService typeService = await db.TypeServices.FindAsync(id);
            if (typeService == null)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            typeService.Name = model.Name;
            typeService.Description = model.Description;
            typeService.Status = model.Status;
            typeService.Image = model.Image;

            db.Entry(typeService).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TypeServiceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TypeServices
        [ResponseType(typeof(TypeService))]
        public async Task<IHttpActionResult> PostTypeService(TypeService typeService)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            typeService.TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt");
           

            db.TypeServices.Add(typeService);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = typeService.Id }, typeService);
        }

        // DELETE: api/TypeServices/5
        [ResponseType(typeof(TypeService))]
        public async Task<IHttpActionResult> DeleteTypeService(long id)
        {
            TypeService typeService = await db.TypeServices.FindAsync(id);
            if (typeService == null)
            {
                return NotFound();
            }

            typeService.Status = Enums.ModelStatus.Delete;

            db.Entry(typeService).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TypeServiceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.Accepted);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TypeServiceExists(long id)
        {
            return db.TypeServices.Count(e => e.Id == id) > 0;
        }
    }
}