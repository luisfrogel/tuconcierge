﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using tuconciergemodels;
using tuconciergerestapi.Models;

namespace tuconciergerestapi.Controllers
{
    [Authorize]
    public class ContractsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;

        public ContractsController()
        {
        }

        public ContractsController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }

        }

        [Route("api/Contracts/App")]

        public async Task<HttpResponseMessage> GetContractsApp(long TypeServiceId = 0, Enums.ModelStatus Status = Enums.ModelStatus.Active, string Name = "")
            
        {
            var user = await UserManager.FindByEmailAsync(User.Identity.GetUserName());

            var data = db.Contracts.Where(x => x.UserId == user.Id);


            //Filtros 
            if(TypeServiceId != 0)
                data = data.Where(x => x.TypeServiceId == TypeServiceId);


            var list = data.Select(x => new HiredServicesViewModel()
            {
                office = db.Offices.Where(z => z.Id == x.OfficeId).FirstOrDefault(),
                Service = db.Services.Where(z => z.Id == x.ServiceId).FirstOrDefault(),
                AttentionStatus = x.AttentionStatus,
            ContractId = x.Id
            }).AsEnumerable();

            if (!Name.Equals(""))
                list = list.Where(x => x.office.Name.Contains(Name));


            return Request.CreateResponse(HttpStatusCode.OK, new { data = list });
        }

        [Route("api/Contracts/App")]

        public async Task<HttpResponseMessage> GetContractsApp(long ContractId)
        {
            Contract contract = await db.Contracts.FindAsync(ContractId);
            if (contract == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, new { message = "NotFound" });
            }

            var model = new ContratedServiceDetailsViewModel()
            {
                AttentionStatus = contract.AttentionStatus,
                Duration = contract.Duration,
                Recurrence = contract.Recurrence,
                StartDate = contract.StartDate,
                StartTime = contract.StartTime,
                TimeStamp = contract.TimeStamp,
                TotalPrice = contract.TotalPrice,
                SalesPlan = db.SalesPlans.Where(z => z.Id == contract.SalesPlanId).FirstOrDefault(),
                Service = db.Services.Where(z => z.Id == contract.ServiceId).FirstOrDefault()
            };

            var listComplements = contract.Complements.Split(',').ToArray();

            model.Complements = db.Complements.Where(x => listComplements.Contains(x.Id.ToString())).Select(x => x.Name).ToList();
            return Request.CreateResponse(HttpStatusCode.OK, new { data = model });
        } 

        // GET: api/Contracts
        [Route("api/Contracts")]
        public async Task<HttpResponseMessage> GetContractsAsync(long OfficeId = 0, Enums.AttentionStatus AttentionStatus = Enums.AttentionStatus.Pending)
        {
            var data = db.Contracts.Where(x => x.Status == Enums.ModelStatus.Active && x.AttentionStatus == AttentionStatus).AsEnumerable();

            if (OfficeId != 0)
            {
                data = data.Where(x => x.OfficeId == OfficeId);
            }
            else
            {
                var user = await UserManager.FindByEmailAsync(User.Identity.GetUserName());

                var usercompany = db.UserCompanies.Where(x => x.UserId == user.Id).FirstOrDefault();

                var offices = db.Offices.Where(x => x.CompanyId == usercompany.CompanyId).Select(x => x.Id).ToArray();

                data = data.Where(x => offices.Contains(x.OfficeId));
            }

            //var list = data.Select(x => new ContractViewModel()
            //{
            //    Id = x.Id,
            //    User = UserManager.Users.Select(z => new UserApp()
            //    {
            //        Id = z.Id,
            //        Name = z.Name,
            //        LastName = z.LastName,
            //        PhoneNumber = z.PhoneNumber,
            //        Birth = z.Birth,
            //        RFC = z.RFC,
            //        UrlImage = z.UrlImage
            //    }).Where(z => z.Id == x.UserId).FirstOrDefault(),

            //    Service = db.Services.Where(z => z.Id == x.ServiceId).FirstOrDefault(),
                
            //    Office = db.Offices.Where(z => z.Id == x.OfficeId).FirstOrDefault(),
            //    SalesPlan = db.SalesPlans.Where(z => z.Id == x.SalesPlanId).FirstOrDefault(),
            //    TypeService = db.TypeServices.Where(z => z.Id == x.TypeServiceId).FirstOrDefault(),
            //    Complements = x.Complements,
            //    Observations = x.Observations,
            //    StartDate = x.StartDate,
            //    StartTime = x.StartTime,
            //    Duration = x.Duration,
            //    Recurrence = x.Recurrence, 
            //    TotalPrice = x.TotalPrice,
            //    DaysSelected = x.DaysSelected,
            //    PaymentMethod = x.PaymentMethod,
            //    PaymentStatus = x.PaymentStatus,
            //    AttentionStatus = x.AttentionStatus,
            //    Calification = x.Calification,
            //    TimeStamp = x.TimeStamp,
            //    Opinion = x.Opinion
            //}).AsEnumerable();

            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        // GET: api/Contracts/5
        [Route("api/Contracts/{id}", Name = "GetContractById")]
        [ResponseType(typeof(Contract))]
        public async Task<IHttpActionResult> GetContract(long id)
        {
            Contract contract = await db.Contracts.FindAsync(id);
            if (contract == null)
            {
                return NotFound();
            }

            return Ok(contract);
        }

        // PUT: api/Contracts/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutContract(long id, Contract model)
        {
            Contract contract = await db.Contracts.FindAsync(id);
            if (contract == null)
            {
                return NotFound();
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            contract.AttentionStatus = model.AttentionStatus;

            db.Entry(contract).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContractExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        private bool ContractExists(long id)
        {
            throw new NotImplementedException();
        }


        // POST: api/Contracts/App
        [Route("api/Contracts")]
        [ResponseType(typeof(ContractPostApp))]
        public async Task<IHttpActionResult> PostContract(ContractPostApp model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await UserManager.FindByEmailAsync(User.Identity.GetUserName());
            var plan = db.SalesPlans.Where(x => x.Id == model.SalesPlanId).FirstOrDefault();
            var service = db.Services.Where(x => x.Id == plan.ServicesId).FirstOrDefault();

            var contract = new Contract();

            contract.Complements = contract.setComplements(model.Complements);
            contract.Observations = model.Observations;
            contract.StartDate = model.StartDate;
            contract.StartTime = model.StartTime;
            contract.Duration = model.Duration;
            contract.Recurrence = model.Recurrence;
            contract.TotalPrice = model.TotalPrice;
            contract.DaysSelected = contract.setDaySelected(model.DaysSelected);
            contract.PaymentMethod = model.PaymentMethod;
            contract.PaymentStatus = model.PaymentStatus;
            //AttentionStatus Default
            contract.SalesPlanId = model.SalesPlanId;
            contract.ServiceId = service.Id;
            contract.TypeServiceId = service.TypeServiceId;
            contract.UserId = user.Id;
            contract.OfficeId = service.IdOffice;
            contract.Opinion = "";
            contract.Calification = 0;
            contract.TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt");

            db.Contracts.Add(contract);

            await db.SaveChangesAsync();

            return CreatedAtRoute("GetContractById", new { id = contract.Id }, contract);
        }

    }
}