﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using tuconciergemodels;
using tuconciergerestapi.Models;

namespace tuconciergerestapi.Controllers
{
    [Authorize]
    public class TypeResidencesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/TypeResidences
        [Authorize(Roles = "root, user")]
        public HttpResponseMessage GetTypeResidences(Enums.ModelStatus Status = Enums.ModelStatus.Active, string Name = "")
        {
            var data = db.TypeResidences.Where(x => x.Status == Status);

            if (!Name.Equals(""))
            {
                data = data.Where(x => x.Name == Name);
            }

            var list = data.Select(x => new TypeResidenceViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                TimeStamp = x.TimeStamp
            }).AsEnumerable();

            return Request.CreateResponse(HttpStatusCode.OK, new { data = list });
        }

        [Authorize(Roles = "root")]
        // GET: api/TypeResidences/5
        [ResponseType(typeof(TypeResidence))]
        public async Task<IHttpActionResult> GetTypeResidence(long id)
        {
            TypeResidence typeResidence = await db.TypeResidences.FindAsync(id);
            if (typeResidence == null)
            {
                return NotFound();
            }

            var viewModel = new TypeResidenceViewModel()
            {
                Id = typeResidence.Id,
                Name = typeResidence.Name,
                Description = typeResidence.Description,
                TimeStamp = typeResidence.TimeStamp
            };

            return Ok(viewModel);
        }

        // PUT: api/TypeResidences/5
        [Authorize(Roles = "root")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTypeResidence(long id, TypeResidence model)
        {
            TypeResidence typeResidence = await db.TypeResidences.FindAsync(id);
            if(typeResidence == null)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            typeResidence.Name = model.Name;
            typeResidence.Description = model.Description;
            db.Entry(typeResidence).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TypeResidenceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TypeResidences
        [ResponseType(typeof(TypeResidence))]
        [Authorize(Roles = "root")]
        public async Task<IHttpActionResult> PostTypeResidence(TypeResidence typeResidence)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            typeResidence.TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt");
            typeResidence.Status = Enums.ModelStatus.Active;

            db.TypeResidences.Add(typeResidence);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = typeResidence.Id }, typeResidence);
        }

        // DELETE: api/TypeResidences/5
        [ResponseType(typeof(TypeResidence))]
        [Authorize(Roles = "root")]
        public async Task<IHttpActionResult> DeleteTypeResidence(long id)
        {
            TypeResidence typeResidence = await db.TypeResidences.FindAsync(id);
            if (typeResidence == null)
            {
                return NotFound();
            }

            typeResidence.Status = Enums.ModelStatus.Delete;

            db.Entry(typeResidence).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TypeResidenceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.Accepted);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TypeResidenceExists(long id)
        {
            return db.TypeResidences.Count(e => e.Id == id) > 0;
        }
    }
}