﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using tuconciergemodels;
using tuconciergerestapi.Models;

namespace tuconciergerestapi.Controllers
{
    [Authorize]
    public class SalesPlansController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public object Status { get; private set; }

        // GET: api/SalesPlans
        public HttpResponseMessage GetSalesPlan(long ServiceId, Enums.ModelStatus Status= Enums.ModelStatus.Active, string Name = "")
        {
            var data = db.SalesPlans.Where(x => x.Status == Status);

            if (!Name.Equals(""))
            {
                data = data.Where(x => x.Name.Contains(Name));
            }
            if (ServiceId != 0)
            {
                data = data.Where(x => x.ServicesId == ServiceId);
            }
            var list = data.Select(x => new SalesPlanViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                Cost = x.Cost,
                TimeStamp = x.TimeStamp,
                ServicesId = x.ServicesId,
                Service = db.Services.Where(z => z.Id == x.ServicesId).FirstOrDefault()
            }).AsEnumerable();
            return Request.CreateResponse(HttpStatusCode.OK, list );
        }

        // GET: api/SalesPlans/5
        [ResponseType(typeof(SalesPlan))]
        public async Task<HttpResponseMessage> GetSalesPlan(long id)
        {
            SalesPlan salesPlan  = await db.SalesPlans.FindAsync(id);
            if (salesPlan == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, new { message = "NotFound" });
            }

            var salesPlanViewModel = new SalesPlanViewModel()
            {
                Id = salesPlan.Id,
                Name = salesPlan.Name,
                Description = salesPlan.Description,
                Cost = salesPlan.Cost,
                TimeStamp = salesPlan.TimeStamp,
                ServicesId = salesPlan.ServicesId,
                Service = db.Services.Where(z => z.Id == salesPlan.ServicesId).FirstOrDefault()

            };

            return Request.CreateResponse(HttpStatusCode.OK, salesPlanViewModel );
        }

        // PUT: api/SalesPlans/5
        [ResponseType(typeof(void))]
        public async Task<HttpResponseMessage> PutSalesPlan(long id, SalesPlan model)
        {
            SalesPlan salesPlan = await db.SalesPlans.FindAsync(id);
            if (salesPlan == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, new { message = "NotFound" });
            }

            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { data = ModelState });
            }

            salesPlan.Name = model.Name;
            salesPlan.Description = model.Description;
            salesPlan.Cost = model.Cost;
            salesPlan.Status = model.Status;
            salesPlan.ServicesId = model.ServicesId;

            db.Entry(salesPlan).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SalesPlanExists(id))
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, new { message = "NotFound" });
                }
                else
                {
                    throw;
                }
            }

            return Request.CreateResponse(HttpStatusCode.OK, new { message = "Done" });
        }

        // POST: api/SalesPlans
        [ResponseType(typeof(SalesPlan))]
        public IHttpActionResult PostSalesPlan(SalesPlan salesPlan)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            salesPlan.TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt");

            db.SalesPlans.Add(salesPlan);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = salesPlan.Id }, salesPlan);
        }

        // DELETE: api/SalesPlans/5
        [ResponseType(typeof(SalesPlan))]
        public async Task<HttpResponseMessage> DeleteSalesPlan(long id)
        {
            SalesPlan salesPlan = await db.SalesPlans.FindAsync(id);
            if (salesPlan == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, new { message = "NotFound" });
            }

            salesPlan.Status = Enums.ModelStatus.Delete;

            db.Entry(salesPlan).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SalesPlanExists(id))
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, new { message = "NotFound" });
                }
                else
                {
                    throw;
                }
            }

            return Request.CreateResponse(HttpStatusCode.Accepted, new { message = "NotFound" });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SalesPlanExists(long id)
        {
            return db.SalesPlans.Count(e => e.Id == id) > 0;
        }
    }
}
