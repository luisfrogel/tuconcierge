﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using tuconciergemodels;
using tuconciergerestapi.Models;
using tuconciergerestapi.Services;

namespace tuconciergerestapi.Controllers
{
    [Authorize]
    public class CompaniesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;

        public CompaniesController()
        {
        }

        public CompaniesController(ApplicationUserManager userManager,
            ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? Request.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        // GET: api/Companies
        [Authorize(Roles = "root")]
        public IQueryable<Company> GetCompanies(Enums.ModelStatus Status = Enums.ModelStatus.Active, string FiscalName = "",
            string ComercialName = "", string RFC = "")
        {
            var data = db.Companies.Where(x => x.Status == Status);

            if (!FiscalName.Equals(""))
            {
                data = data.Where(x => x.FiscalName.Contains(FiscalName));
            }

            if (!ComercialName.Equals(""))
            {
                data = data.Where(x => x.ComercialName.Contains(ComercialName));
            }

            if (!RFC.Equals(""))
            {
                data = data.Where(x => x.RFC.Contains(RFC));
            }

            return data;
        }

        // GET: api/Companies/5
        [Authorize(Roles = "root")]
        [ResponseType(typeof(CompanyFormModel))]
        public async Task<IHttpActionResult> GetCompany(long id)
        {
            Company company = await db.Companies.FindAsync(id);
            if (company == null)
            {
                return NotFound();
            }

            var adminCompany = db.adminCompanies.Where(x => x.CompanyId == id).FirstOrDefault();

            var user = await UserManager.FindByIdAsync(adminCompany.UserId);

            var companyFormModel = new CompanyFormModel()
            {
                Id = company.Id,
                ComercialName = company.ComercialName,
                FiscalName = company.FiscalName,
                FiscalRegime = company.FiscalRegime,
                RFC = company.RFC,
                RowVersion = company.RowVersion,
                Status = company.Status,
                TimeStamp = company.TimeStamp,
                AdminEmail = user.Email,
                AdminId = user.Id,
                AdminLastName = user.LastName,
                AdminName = user.Name,
                AdminPhoneNumber = user.PhoneNumber
            };

            return Ok(companyFormModel);
        }

        // PUT: api/Companies/5
        [Authorize(Roles = "root")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCompany(long id, CompanyFormModel model)
        {
            Company company = await db.Companies.FindAsync(id);
            if (company == null)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            company.FiscalName = model.FiscalName;
            company.ComercialName = model.ComercialName;
            company.FiscalRegime = model.FiscalRegime;
            company.RFC = model.RFC;
            company.Status = model.Status;

            db.Entry(company).State = EntityState.Modified;

            var adminCompany = db.adminCompanies.Where(x => x.CompanyId == id).FirstOrDefault();

            var user = await UserManager.FindByIdAsync(adminCompany.UserId);

            user.Name = model.AdminName;
            user.LastName = model.AdminLastName;
            user.PhoneNumber = model.AdminPhoneNumber;

            await UserManager.UpdateAsync(user);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompanyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Companies
        [Authorize(Roles = "root")]
        [ResponseType(typeof(Company))]
        public async Task<IHttpActionResult> PostCompany(CompanyFormModel formModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Company company = new Company()
            {
                ComercialName = formModel.ComercialName,
                FiscalName = formModel.FiscalName,
                FiscalRegime = formModel.FiscalRegime,
                RFC = formModel.RFC,
                RowVersion = formModel.RowVersion,
                Status = formModel.Status,
                TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt")
            };

            db.Companies.Add(company);
            await db.SaveChangesAsync();

            // Add User

            var code = EmailJetService.GenerateCodeVerification();
            var password = "!" + PasswordService.GeneratePassword(19);

            var user = new ApplicationUser()
            {
                UserName = formModel.AdminEmail,
                Email = formModel.AdminEmail,
                Name = formModel.AdminName,
                LastName = formModel.AdminLastName,
                PhoneNumber = formModel.AdminPhoneNumber,
                Birth = "",
                CURP = "",
                Gender = Enums.Gender.Masculino,
                MaritalState = Enums.MaritalState.Soltero,
                RFC = "",
                UrlImage = "https://res.cloudinary.com/lfrr/image/upload/v1572807157/usertuconcierge.png",
                EmailConfirmed = false,
                CodeConfirmEmail = code
            };

            IdentityResult result = await UserManager.CreateAsync(user, password);

            if (result.Succeeded)
            {
                var UserRegistrated = await UserManager.FindByEmailAsync(user.Email);

                await UserManager.AddToRoleAsync(UserRegistrated.Id, "admin");

                var link = string.Format("{0}/Account/Activation?Email={1}&Code={2}", ConfigurationManager.AppSettings["WebDomain"].ToString(), user.Email, code);

                var HTMLPart = EmailJetService.GenerateEmailAdminActivation(user.Email, password, link);

                try
                {
                    await EmailJetService.SendEmail(formModel.AdminEmail, string.Format("{0} {1}", formModel.AdminName, formModel.AdminLastName),
                        "¡Bienvenido a nuestra plaforma!", HTMLPart);
                }
                catch (Exception) { }

                // Add AdminCompany Table

                var adminCompany = new AdminCompany()
                {
                    CompanyId = company.Id,
                    UserId = user.Id
                };

                var usercompany = new UserCompany()
                {
                    CompanyId = company.Id,
                    UserId = user.Id
                };

                db.UserCompanies.Add(usercompany);
                db.adminCompanies.Add(adminCompany);

                await db.SaveChangesAsync();

                return CreatedAtRoute("DefaultApi", new { id = company.Id }, formModel);
            }
            else
            {
                return CreatedAtRoute("DefaultApi", new { id = company.Id }, company);
            }
        }

        // DELETE: api/Companies/5
        [Authorize(Roles = "root")]
        [ResponseType(typeof(Company))]
        public async Task<IHttpActionResult> DeleteCompany(long id)
        {
            Company company = await db.Companies.FindAsync(id);
            if (company == null)
            {
                return NotFound();
            }

            company.Status = Enums.ModelStatus.Delete;
            db.Entry(company).State = EntityState.Modified;

            var adminCompany = db.adminCompanies.Where(x => x.CompanyId == id).FirstOrDefault();

            var user = await UserManager.FindByIdAsync(adminCompany.UserId);

            user.EmailConfirmed = false;
            await UserManager.UpdateAsync(user);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompanyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.Accepted);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CompanyExists(long id)
        {
            return db.Companies.Count(e => e.Id == id) > 0;
        }
    }
}