﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using tuconciergemodels;
using tuconciergerestapi.Models;

namespace tuconciergerestapi.Controllers
{
    [Authorize]
    public class StaffsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;

        public StaffsController()
        {
        }

        public StaffsController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: api/Staffs
        public async Task<IEnumerable<StaffViewModel>> GetStaffsAsync(long OfficeId = 0, Enums.ModelStatus Status = Enums.ModelStatus.Active,string Name = "", string FatherLastname= "", string MotherLastname = "", string RFC = "", string CURP ="")
        {
            var data = db.Staffs.Where(x => x.Status == Status);

            if (!Name.Equals(""))
                data = data.Where(x => x.Name.Contains(Name));

            if (!FatherLastname.Equals(""))
                data = data.Where(x => x.FatherLastName.Contains(FatherLastname));

            if (!MotherLastname.Equals(""))
                data = data.Where(x => x.MotherLastName.Contains(MotherLastname));

            if (!RFC.Equals(""))
                data = data.Where(x => x.RFC.Contains(RFC));

            if (!CURP.Equals(""))
                data = data.Where(x => x.CURP.Contains(CURP));

            if(OfficeId != 0)
            {
                data = data.Where(x => x.OfficeId ==  OfficeId);
            }
            else
            {
                var user = await UserManager.FindByEmailAsync(User.Identity.GetUserName());

                var usercompany = db.UserCompanies.Where(x => x.UserId == user.Id).FirstOrDefault();

                var offices = db.Offices.Where(x => x.CompanyId == usercompany.CompanyId).Select(x => x.Id).ToArray();

                data = data.Where(x => offices.Contains(x.OfficeId));
            }

            var list = data.Select(x => new StaffViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                FatherLastName = x.FatherLastName,
                MotherLastName = x.MotherLastName,
                Address = x.Address,
                Birth = x.Birth,
                City = x.City,
                CURP = x.CURP,
                Email = x.Email,
                Gender = x.Gender,
                MaritalState = x.MaritalState,
                OfficeId = x.OfficeId,
                PhoneNumber = x.PhoneNumber,
                RFC = x.RFC,
                StateId = x.StateId,
                Status = x.Status,
                TimeStamp = x.TimeStamp, 
                Photo = x.Photo,
                Office = db.Offices.Where(z => z.Id == x.OfficeId).FirstOrDefault(),
                State = db.States.Where(z => z.Id == x.StateId).FirstOrDefault()
            });
            return list;
        }

        // GET: api/Staffs/5
        [ResponseType(typeof(Staff))]
        public async Task<IHttpActionResult> GetStaff(long id)
        {
            Staff x = await db.Staffs.FindAsync(id);
            if (x == null)
            {
                return NotFound();
            }

            var data = new StaffViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                FatherLastName = x.FatherLastName,
                MotherLastName = x.MotherLastName,
                Address = x.Address,
                Birth = x.Birth,
                City = x.City,
                CURP = x.CURP,
                Email = x.Email,
                Gender = x.Gender,
                MaritalState = x.MaritalState,
                OfficeId = x.OfficeId,
                PhoneNumber = x.PhoneNumber,
                RFC = x.RFC,
                StateId = x.StateId,
                Status = x.Status,
                TimeStamp = x.TimeStamp,
                Photo = x.Photo,
                Office = db.Offices.Where(z => z.Id == x.OfficeId).FirstOrDefault(),
                State = db.States.Where(z => z.Id == x.StateId).FirstOrDefault()
            };

            return Ok(data);
        }

        // PUT: api/Staffs/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutStaff(long id, Staff model)
        {
            var staff = await db.Staffs.FindAsync(id);

            if (staff == null)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            staff.Name = model.Name;
            staff.FatherLastName = model.FatherLastName;
            staff.MotherLastName = model.MotherLastName;
            staff.Birth = model.Birth;
            staff.PhoneNumber = model.PhoneNumber;
            staff.Email = model.Email;
            staff.RFC = model.RFC;
            staff.CURP = model.CURP;
            staff.Gender = model.Gender;
            staff.MaritalState = model.MaritalState;
            staff.Address = model.Address;
            staff.City = model.City;
            staff.OfficeId = model.OfficeId;
            staff.StateId = model.StateId;
            staff.Status = model.Status;
            staff.Photo = model.Photo;


            db.Entry(staff).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StaffExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Staffs
        [ResponseType(typeof(Staff))]
        public async Task<IHttpActionResult> PostStaff(Staff staff)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            staff.TimeStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt");


            db.Staffs.Add(staff);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = staff.Id }, staff);
        }

        // DELETE: api/Staffs/5
        [ResponseType(typeof(Staff))]
        public async Task<IHttpActionResult> DeleteStaff(long id)
        {
            Staff staff = await db.Staffs.FindAsync(id);
            if (staff == null)
            {
                return NotFound();
            }

            staff.Status = Enums.ModelStatus.Delete;

            db.Entry(staff).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StaffExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.Accepted);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool StaffExists(long id)
        {
            return db.Staffs.Count(e => e.Id == id) > 0;
        }
    }
}