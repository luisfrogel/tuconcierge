﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using tuconciergemodels;

namespace tuconciergerestapi.Models
{
    // Para agregar datos de perfil del usuario, agregue más propiedades a su clase ApplicationUser. Visite https://go.microsoft.com/fwlink/?LinkID=317594 para obtener más información.
    public class ApplicationUser : IdentityUser
    {
        [Required]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Display(Name = "Apellidos")]
        public string LastName { get; set; }

        [Display(Name = "Fecha de cumpleaños")]
        public string Birth { get; set; }

        [Display(Name = "Género")]
        public Enums.Gender Gender { get; set; }

        [Display(Name = "Estado cívil")]
        public Enums.MaritalState MaritalState { get; set; }

        [Display(Name = "CURP")]
        public string CURP { get; set; }

        [Display(Name = "RFC")]
        public string RFC { get; set; }

        [Display(Name = "Foto")]
        public string UrlImage { get; set; }

        public string CodeConfirmEmail { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(ApplicationUserManager userManager, string authenticationType)
        {
            // Tenga en cuenta que el valor de authenticationType debe coincidir con el definido en CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await userManager.CreateIdentityAsync(this, authenticationType);
            // Agregar aquí notificaciones personalizadas de usuario
            return userIdentity;
        }

    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, IdentityRole, string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim>
    {
        public DbSet<Company> Companies { get; set; }
        public DbSet<Office> Offices { get; set; }
        public DbSet<Coverage> Coverages { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<TypeService> TypeServices { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<Staff> Staffs { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<CoverageServices> CoverageServices { get; set; }
        public DbSet<StaffServices> StaffServices { get; set; }
        public DbSet<Residences> Residences { get; set; }
        public DbSet<TypeResidence> TypeResidences { get; set; }
        public DbSet<SalesPlan> SalesPlans { get; set; }
        public DbSet<AdminCompany> adminCompanies { get; set; }
        public DbSet<UserCompany> UserCompanies { get; set; }
        public DbSet<Complement> Complements { get; set; }
        public DbSet<Contract> Contracts { get; set; }

        public DbSet<StaffContract> StaffContracts { get; set; }
        public ApplicationDbContext()
            : base("TuconciergeDbConnection")
        {
        }
        
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}