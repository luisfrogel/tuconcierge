﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tuconciergemodels
{
    public class StaffServices
    {
        public long Id { get; set; }

        [ForeignKey("StaffId")]
        public virtual Staff Staff { get; set; }
        public long StaffId { get; set; }

        [ForeignKey("ServiceId")]
        public virtual Service Service { get; set; }
        public long ServiceId { get; set; }
    }
}
