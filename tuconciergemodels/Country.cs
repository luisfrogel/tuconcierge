﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tuconciergemodels
{
    public class Country: IModel
    {
        public long Id { get; set; }

        [Required]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }
    }
}
