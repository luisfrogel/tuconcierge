﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tuconciergemodels
{
   public class SalesPlan: IModel
    {
        public long Id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Description")]
        [Column(TypeName = "text")]
        public string Description { get; set; }

        [Display(Name = "Cost")]
        public double Cost { get; set; }

        public long ServicesId {get; set;}

    }

    public class SalesPlanViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Cost { get; set; }
        public string TimeStamp { get; set; }
        public long ServicesId { get; set; }
        public Service Service { get; set; }
    }
}
