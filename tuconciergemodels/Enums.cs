﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tuconciergemodels
{
    public class Enums
    {
        public enum Authentication
        {
            NONE,
            DEFAULT
        }

        public enum ModelStatus
        {
            Active,
            Delete
        }

        public enum MaritalState
        {
            [Display(Name = "Soltero")]
            Soltero,
            [Display(Name = "Comprometido")]
            Comprometido,
            [Display(Name = "Casado")]
            Casado,
            [Display(Name = "Unión Libre")]
            Union_Libre,
            [Display(Name = "Divorciado")]
            Divorciado,
            [Display(Name = "Viudo")]
            Viudo
        }

        public enum Gender
        {
            [Display(Name = "Masculino")]
            Masculino,
            [Display(Name = "Femenino")]
            Femenino
        }

        public enum Days
        {
            [Display(Name = "Lunes")]
            Monday,
            [Display(Name = "Martes")]
            Tuesday,
            [Display(Name = "Miercoles")]
            Wednesday,
            [Display(Name = "Jueves")]
            Thursday,
            [Display(Name = "Viernes")]
            Friday,
            [Display(Name = "Sábado")]
            Saturday,
            [Display(Name = "Domingo")]
            Sunday
        }

        public enum PaymentStatus
        {
            [Display(Name = "Pendiente")]
            Pending,
            [Display(Name ="Pagado")]
            PaidOut
        }

        public enum AttentionStatus
        {
            [Display(Name = "Pendiente")]
            Pending,
            [Display(Name = "Asignado")]
            Assigned,
            [Display(Name = "Iniciado")]
            Started,
            [Display(Name = "Finalizado")]
            Finished
        }

        public enum ContactStatus
        {
            Pendiente,
            Contactado,
            Descartado
        }

        public AttentionStatus attentionStatus { get; set; }
    }
}
