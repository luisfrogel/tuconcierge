﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tuconciergemodels
{
    public class AdminCompany
    {
        [Key, Column(Order = 0)]
        public long CompanyId { get; set; }

        [Key, Column(Order = 1)]
        public string UserId { get; set; }
    }
}
