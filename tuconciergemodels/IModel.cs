﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tuconciergemodels
{
    public abstract class IModel
    {
        [DefaultValue(Enums.ModelStatus.Active)]
        public Enums.ModelStatus Status { get; set; }

        public string TimeStamp { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}
