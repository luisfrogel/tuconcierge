﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tuconciergemodels
{
    public class Service: IModel
    {
        public long Id { get; set; }

        [Required]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Display(Name = "Descripción")]
        [Column(TypeName = "text")]
        public string Description { get; set; }

        [Display(Name = "Observaciones")]
        [Column(TypeName = "text")]
        public string Observaciones { get; set; }

        [Display(Name = "Recordatorios")]
        [Column(TypeName = "text")]
        public string Recordatorios { get; set; }

        public long TypeServiceId { get; set; }

        public long IdOffice { get; set; }
    }

    public class ServiceViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Observaciones { get; set; }
        public string Recordatorios { get; set; }
        public long TypeServiceId { get; set; }
        public TypeService TypeService { get; set; }
        public long IdOffice { get; set; }
        public Office Office { get; set; }
        public string TimeStamp { get; set; }
    }

    public class ServiceAppViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Observaciones { get; set; }
        public string Recordatorios { get; set; }
        public TypeService TypeService { get; set; }
        public Office Office { get; set; }
        public string TimeStamp { get; set; }

        public IEnumerable<SalesPlansAppViewModel> Plans { get; set; }
    }

    public class SalesPlansAppViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Cost { get; set; }

        public IEnumerable<ComplementAppViewModel> Complements { get; set; }

    }

    public class ComplementAppViewModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public double Cost { get; set; }
    }
}
