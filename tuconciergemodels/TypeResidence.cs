﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tuconciergemodels
{
    public class TypeResidence : IModel
    {
        public long Id { get; set; }

        [Required]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Display(Name = "Descripción")]
        [Column(TypeName = "text")]
        public string Description { get; set; }
    }

    public class TypeResidenceViewModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string TimeStamp { get; set; }
    }
}