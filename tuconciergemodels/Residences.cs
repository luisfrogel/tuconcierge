﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tuconciergemodels
{
    public class Residences : IModel
    {

        public long Id { get; set; }

        [Required]
        public string Location { get; set; }

        [Required]
        public string Longitude { get; set; }

        [Required]
        public string Latitude { get; set; }

        public string InternalNumber { get; set; }

        public string ExternalNumber { get; set; }

        public string Street { get; set; }

        public string References { get; set; }

        public string Crossings { get; set; }

        public virtual TypeResidence TypeResidence { get; set; }

        public long TypeResidenceId { get; set; }

        [Required]
        public string UserId { get; set; }
    }

    public class ResidenceViewModel
    {

        public long Id { get; set; }

        [Required]
        [Display(Name = "Ubicación")]
        public string Location { get; set; }

        [Required]
        [Display(Name = "Longitud")]
        public string Longitude { get; set; }

        [Required]
        [Display(Name = "Latitud")]
        public string Latitude { get; set; }

        [Display(Name = "Número interior")]
        public string InternalNumber { get; set; }

        [Display(Name = "Númerio exterior")]
        public string ExternalNumber { get; set; }

        [Display(Name = "Calle")]
        public string Street { get; set; }

        [Display(Name = "Referencias")]
        public string References { get; set; }

        [Display(Name = "Cruzamientos")]
        public string Crossings { get; set; }

        [ForeignKey("TypeResidenceId")]
        public virtual TypeResidence TypeResidence { get; set; }

        [Required]
        public string UserId { get; set; }

        public string TimeStamp { get; set; }
    }
}
