﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tuconciergemodels
{
    public class Contract : IModel
    {
        public long Id { get; set; }

        [Required]
        public string UserId { get; set; }

        public long OfficeId { get; set; }

        [Required]
        public long SalesPlanId { get; set; }

        public long ServiceId { get; set; }
        public long TypeServiceId { get; set; }

        [Required]
        public string Complements { get; set; }

        public string Observations { get; set; }

        [Required]
        public string StartDate { get; set; }

        [Required]
        public string StartTime { get; set; }

        [Required]
        public long Duration { get; set; }

        public string Recurrence { get; set; }

        [Required]
        public double TotalPrice { get; set; }

        public string DaysSelected { get; set; }

        public string PaymentMethod { get; set; }

        public Enums.PaymentStatus PaymentStatus { get; set; }

        [DefaultValue(Enums.AttentionStatus.Pending)]
        public Enums.AttentionStatus AttentionStatus { get; set; }

        public long Calification { get; set; }

        [Column(TypeName = "text")]
        public string Opinion { get; set; }

        public string setDaySelected(List<Enums.Days> days)
        {
            return string.Join(",", days.ToArray());
        }

        public string setComplements(List<long> complements)
        {
            return string.Join(",", complements.ToArray());
        }

    }

    public class ContractViewModel
    {
        public long Id { get; set; }

        public UserApp User { get; set; }

        public Office Office { get; set; }

        public SalesPlan SalesPlan { get; set; }

        public Service Service { get; set; }
        public TypeService TypeService { get; set; }

        public string Complements { get; set; }

        public string Observations { get; set; }

        public string StartDate { get; set; }

        public string StartTime { get; set; }

        public long Duration { get; set; }

        public string Recurrence { get; set; }

        public double TotalPrice { get; set; }

        public string DaysSelected { get; set; }

        public string PaymentMethod { get; set; }

        public Enums.PaymentStatus PaymentStatus { get; set; }

        public Enums.AttentionStatus AttentionStatus { get; set; }

        public long Calification { get; set; }

        public string Opinion { get; set; }

        public string TimeStamp { get; set; }
    }

    public class ContractPostApp
    {
        [Required]
        public long SalesPlanId { get; set; }

        public List<long> Complements { get; set; }

        public string Observations { get; set; }

        [Required]
        public string StartDate { get; set; }

        [Required]
        public string StartTime { get; set; }

        [Required]
        public long Duration { get; set; }

        public string Recurrence { get; set; }

        [Required]
        public double TotalPrice { get; set; }

        public List<Enums.Days> DaysSelected { get; set; }

        [Required]
        public Enums.PaymentStatus PaymentStatus { get; set; }

        public string PaymentMethod { get; set; }

    }

    public class HiredServicesViewModel
    { 
    
    public Office office { get; set; }

    public Service Service { get; set; }

     public Enums.AttentionStatus AttentionStatus { get; set; }

    public long ContractId { get; set; }

    
    }

    public class ContratedServiceDetailsViewModel 
    {
        public string StartDate { get; set; }
        public string TimeStamp { get; set; }

        public string StartTime { get; set; }
        public long Duration { get; set; }
        public string Recurrence { get; set; }
        public double TotalPrice { get; set; }
        public Enums.AttentionStatus AttentionStatus { get; set; }
        public List<string> Complements { get; set; }
        public  Service Service { get; set; }
        public SalesPlan SalesPlan { get; set; }

        public List<AssignedStaffViewModel> AssignedStaffs { get; set; }

    }

    public class AssignedStaffViewModel
    {
        public string Name { get; set; }
        public string Photo { get; set; }
    }





}
