﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tuconciergemodels
{
    public class Office : IModel
    {
        public long Id { get; set; }

        [Required]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Display(Name = "Logo")]
        public string Picture { get; set; }

        [Display(Name = "Latitud")]
        public string Lat { get; set; }

        [Display(Name = "Logitud")]
        public string Long { get; set; }

        [Display(Name = "Dirección")]
        public string Address { get; set; }

        [Display(Name = "Código postal")]
        public string PostalCode { get; set; }

        [Display(Name = "Ciudad")]
        public string City { get; set; }

        public long StateId { get; set; }

        public long CompanyId { get; set; }

        public long CountryId { get; set; }
    }

    public class OfficeViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Picture { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public long StateId { get; set; }
        public long CompanyId { get; set; }
        public long CountryId { get; set; }
    }
}
