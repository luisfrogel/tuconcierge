﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tuconciergemodels
{
    public class LoginUser
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }

    public class UserViewModel
    {
        public string Id { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public string Birth { get; set; }

        public Enums.Gender Gender { get; set; }

        public Enums.MaritalState MaritalState { get; set; }

        public string CURP { get; set; }

        public string RFC { get; set; }

        public string UrlImage { get; set; }
    }

    public class VerifyEmailModel
    {
        [Required]
        [Display(Name = "Correo electrónico")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Código")]
        public string Code { get; set; }
    }

    public class UserApp
    {
        public string Id { get; set; }

        [Required]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Apellidos")]
        public string LastName { get; set; }

        [Display(Name = "Teléfono")]
        public string PhoneNumber { get; set; }

        [Display(Name = "RFC")]
        public string RFC { get; set; }

        [Display(Name = "Fecha de nacimiento")]
        public string Birth { get; set; }

        [Display(Name = "Foto")]
        public string UrlImage { get; set; }
    }

    public class userviewweb
    {
        public string Id { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public string Birth { get; set; }

        public string Gender { get; set; }

        public string MaritalState { get; set; }

        public string CURP { get; set; }

        public string RFC { get; set; }

        public string UrlImagen { get; set; }

        public string RolId { get; set; }

        public string RolName { get; set; }
    }
}
