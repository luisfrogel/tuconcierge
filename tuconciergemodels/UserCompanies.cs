﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tuconciergemodels
{
    public class UserCompany
    {
        public long Id { get; set; }

        public string UserId { get; set; }

        public long CompanyId { get; set; }

    }

    public class UserCompanyViewModel
    {
        public string UserId { get; set; }

        public Company Company { get; set; }
    }
}
