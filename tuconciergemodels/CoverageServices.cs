﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tuconciergemodels
{
    public class CoverageServices
    {
        public long Id { get; set; }

        [ForeignKey("CoverageId")]
        public virtual Coverage Coverage { get; set; }
        public long CoverageId { get; set; }

        [ForeignKey("ServiceId")]
        public virtual Service Service { get; set; }
        public long ServiceId { get; set; }
    }
}
