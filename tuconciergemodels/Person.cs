﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tuconciergemodels
{
    public class Person : IModel
    {
        [Required]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Apellido Paterno")]
        public string FatherLastName { get; set; }

        [Required]
        [Display(Name = "Apellido Materno")]
        public string MotherLastName { get; set; }

        [Display(Name = "Fecha de cumpleaños")]
        public string Birth { get; set; }

        public Enums.Gender Gender { get; set; }

        public Enums.MaritalState MaritalState { get; set; }

        [Display(Name = "CURP")]
        public string CURP { get; set; }

        [Display(Name = "RFC")]
        public string RFC { get; set; }
    }
}
