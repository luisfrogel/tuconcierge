﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tuconciergemodels
{
    public class Coverage : IModel
    {
        public long Id { get; set; }

        [Required]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Radio")]
        public string Ratio { get; set; }

        [Required]
        [Display(Name = "Latitud")]
        public string Lat { get; set; }

        [Required]
        [Display(Name = "Logitud")]
        public string Long { get; set; }

        public long OfficeId { get; set; }

    }

    public class CoverageViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Ratio { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public long OfficeId { get; set; }
        public Office Office { get; set; }
        public string TimeStamp { get; set; }
    }

    public class Coordinates
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class CoverageOffice : Coordinates
    {
        public long CoverageId { get; set; }
        public long OfficeId { get; set; }

        public double Ratio { get; set; }
    }
}
