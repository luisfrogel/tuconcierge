﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tuconciergemodels
{
    public class IResponseActions
    {
        public bool Success { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string TimeStamp { get; set; }
    }

    public class IResponseItems<T>
    {
        public bool Success { get; set; }
        public T Data { get; set; }
        public string TimeStamp { get; set; }
    }
}
