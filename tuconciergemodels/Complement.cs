﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using tuconciergemodels;

namespace tuconciergerestapi.Models
{
    public class Complement : IModel
    {

        public long Id { get; set; }

        [Required]
        [Display(Name ="Nombre")]
        public string Name { get; set; }

        [Display(Name = "Nombre")]
        [Column(TypeName = "text")]
        public string Description { get; set; }

        [Required]
        public double Cost { get; set; }

        [Required]
        public long SalesPlanId { get; set; }
    }

    public class ComplementViewModel
    {

        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public double Cost { get; set; }

        public SalesPlan SalesPlans { get; set; }

        public string TimeStamp { get; set; }
    }
}

