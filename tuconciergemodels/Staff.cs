﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tuconciergemodels
{
    public class Staff : Person
    {
        public long Id { get; set; }

        [EmailAddress(ErrorMessage = "Correo electrónico no válido")]
        [Display(Name = "Correo electrónico")]
        public string Email { get; set; }

        [Display(Name = "Teléfono")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Dirección")]
        public string Address { get; set; }

        [Display(Name = "Ciudad")]
        public string City { get; set; }

        [Display(Name = "Foto")]
        public string Photo { get; set; }

        public long StateId { get; set; }

        public long OfficeId { get; set; }
    }

    public class StaffViewModel : Person
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Photo { get; set; }
        public State State { get; set; }
        public long StateId { get; set; }
        public Office Office { get; set; }
        public long OfficeId { get; set; }
    }
}
