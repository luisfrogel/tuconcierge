﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace tuconciergemodels
{
    public class TypeService : IModel
    {
        public long Id { get; set; }

        [Required]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Display(Name = "Descripción")]
        [Column(TypeName = "text")]
        public string Description { get; set; }

        public string Image { get; set; }

    }

    public class TypeServiceViewModel
    {
        
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public HttpPostedFileBase File { get; set; }

        [System.ComponentModel.DefaultValue(Enums.ModelStatus.Active)]
        public Enums.ModelStatus Status { get; set; }

    }

}





