﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tuconciergemodels
{
    public class Company : IModel
    {
        public long Id { get; set; }

        [Required]
        [Display(Name ="Nombre fiscal")]
        public string FiscalName { get; set; }

        [Required]
        [Display(Name = "Nombre comercial")]
        public string ComercialName { get; set; }

        [Required]
        [Display(Name = "RFC")]
        public string RFC { get; set; }

        [Display(Name = "Regimen fiscal")]
        public string FiscalRegime { get; set; }
    }

    public class CompanyFormModel : IModel
    {
        public long Id { get; set; }

        [Required]
        [Display(Name = "Nombre fiscal")]
        public string FiscalName { get; set; }

        [Required]
        [Display(Name = "Nombre comercial")]
        public string ComercialName { get; set; }

        [Required]
        [Display(Name = "RFC")]
        public string RFC { get; set; }

        [Display(Name = "Regimen fiscal")]
        public string FiscalRegime { get; set; }

        [Required]
        [Display(Name = "Nombre")]
        public string AdminName { get; set; }

        [Required]
        [Display(Name = "Apellido")]
        public string AdminLastName { get; set; }

        [Required]
        [Display(Name = "Correo electrónico")]
        public string AdminEmail { get; set; }

        [Display(Name = "Número de teléfono")]
        public string AdminPhoneNumber { get; set; }

        public string AdminId { get; set; }
    }

}
